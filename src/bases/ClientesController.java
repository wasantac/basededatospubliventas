/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Cliente;
import basesObjetos.Conexion;
import basesObjetos.Persona;
import basesObjetos.ClienteNatural;
import basesObjetos.ClienteJuridico;
import basesObjetos.Contrato;
import basesObjetos.Detalle;
import basesObjetos.Pago;
import basesObjetos.Paquete;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class ClientesController implements Initializable {

    @FXML
    private Button Atras;
    @FXML
    private TextField buscarField;
    @FXML
    private TableView<ClienteNatural> tablaClientes;
    @FXML
    private TableColumn<ClienteNatural, String> columnaCedula;
    @FXML
    private TableColumn<ClienteNatural, String> ColumnaConercial;
    @FXML
    private TableColumn<ClienteNatural, String> columnaNombre;
    @FXML
    private TableColumn<ClienteNatural, String> columnaApellido;
    @FXML
    private TableColumn<ClienteNatural, String> columnaTelefono;
    @FXML
    private TableColumn<ClienteNatural, String> columnaDireccion;
    @FXML
    private TableColumn<ClienteNatural, String> ColumnaActividad;

    @FXML
    private Button nuevoCliente;
    @FXML
    private Button modificar;
    @FXML
    private Button eliminar;
    
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    public static ObservableList<ClienteNatural> clientes = Bases.clientes;
    public static ObservableList<ClienteJuridico> clientesJ = Bases.clientesJ;
    private ObservableList<Persona> personas = FXCollections.observableArrayList();
    @FXML
    private TableColumn<ClienteJuridico, String> cedulaJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> nombreJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> apellidoJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> telefonoJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> direccionJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> nombreComercialJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> actividadComercialJuridico;
    @FXML
    private TableColumn<ClienteJuridico, String> razonSocial;
    @FXML
    private TabPane TBPane;
    @FXML
    private Tab tabCliente;
    @FXML
    private Tab tabClienteJ;
    @FXML
    private TableView<ClienteJuridico> tablaClientesJuridico;
    @FXML
    private TextField CedulaModField;
    @FXML
    private TextField NombreModField;
    @FXML
    private TextField ApellidoModField;
    @FXML
    private TextField TelefonoModField;
    @FXML
    private TextField DireccionModField;
    @FXML
    private TextField ComercialModField;
    @FXML
    private TextField ActComercModField;
    @FXML
    private TextField RazonModField;


    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Conecta con la base de datos y da valores en las columnas de la tabla

        //Sirve para hacer busquedas filtradas y encontrar los datos que se quieran ver
        setCellTable();
        setCellTableJuridico();
        cargarFiltros();

    }
    
    

    //Cambia a la pantalla principal
    @FXML
    private void changeScreen (ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
        clearSelect();
    }
    //Abre nueva ventana para crear un cliente
    @FXML
    private void crearCliente(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("NuevoCliente.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("Nuevo Cliente");
        stage.show();
        clearSelect();
        Thread thread = new Thread(){
          public void run(){
            System.out.println("Thread Running");
            while(stage.isShowing()){
                DesactivarBoton();
            }
            ActivarBoton();
          }
          
        };
        thread.start();
        
    //Se usa para la creacion de las columnas   
    }
    private void setCellTable(){
        this.columnaCedula.setCellValueFactory(new PropertyValueFactory("Cedula"));
        this.columnaNombre.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.columnaApellido.setCellValueFactory(new PropertyValueFactory("Apellido"));
        this.columnaTelefono.setCellValueFactory(new PropertyValueFactory("Telefono"));
        this.columnaDireccion.setCellValueFactory(new PropertyValueFactory("Direccion"));
        this.ColumnaConercial.setCellValueFactory(new PropertyValueFactory("NombreComercial"));
        this.ColumnaActividad.setCellValueFactory(new PropertyValueFactory("ActividadComercial"));
    //carga los datos de la base de datos y los carga en la tabla    
    }
    private void setCellTableJuridico(){
        this.cedulaJuridico.setCellValueFactory(new PropertyValueFactory("Cedula"));
        this.nombreJuridico.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.apellidoJuridico.setCellValueFactory(new PropertyValueFactory("Apellido"));
        this.telefonoJuridico.setCellValueFactory(new PropertyValueFactory("Telefono"));
        this.direccionJuridico.setCellValueFactory(new PropertyValueFactory("Direccion"));
        this.nombreComercialJuridico.setCellValueFactory(new PropertyValueFactory("NombreComercial"));
        this.actividadComercialJuridico.setCellValueFactory(new PropertyValueFactory("ActividadComercial"));
        this.razonSocial.setCellValueFactory(new PropertyValueFactory("RazonSocial"));
    //carga los datos de la base de datos y los carga en la tabla    
    }

    

    @FXML
    private void modificarFila(ActionEvent event) throws Exception {
        System.out.println("Modificar Cliente");
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        pst.execute();
        if(tabCliente.isSelected()){
            String query  = "call actualizarClienteNatural(?,?,?,?,?,?,?);";
            pst = con.getCon().prepareStatement(query);
            pst.setString(1,CedulaModField.getText());
            pst.setString(2, NombreModField.getText());
            pst.setString(3, ApellidoModField.getText());
            pst.setString(4, TelefonoModField.getText());
            pst.setString(5, DireccionModField.getText());
            pst.setString(6, ActComercModField.getText());
            pst.setString(7,ComercialModField.getText());
            pst.execute();
            ClienteNatural c = tablaClientes.getSelectionModel().getSelectedItem();
            c.setNombre(NombreModField.getText());
            c.setApellido(ApellidoModField.getText());
            c.setTelefono(TelefonoModField.getText());
            c.setDireccion(DireccionModField.getText());
            c.setActividadComercial(ActComercModField.getText());
            c.setNombreComercial(ComercialModField.getText());
            tablaClientes.getSelectionModel().clearSelection();
            
        }
        if(tabClienteJ.isSelected()){
            String query  = "call actualizarClienteJuridico(?,?,?,?,?,?,?,?);";
            pst = con.getCon().prepareStatement(query);
            pst.setString(1,CedulaModField.getText() );
            pst.setString(2, NombreModField.getText());
            pst.setString(3, ApellidoModField.getText());
            pst.setString(4, TelefonoModField.getText());
            pst.setString(5, DireccionModField.getText());
            pst.setString(6, ActComercModField.getText());
            pst.setString(7,ComercialModField.getText());
            pst.setString(8,RazonModField.getText());

            pst.execute();
            ClienteJuridico c = tablaClientesJuridico.getSelectionModel().getSelectedItem();
            c.setNombre(NombreModField.getText());
            c.setApellido(ApellidoModField.getText());
            c.setTelefono(TelefonoModField.getText());
            c.setDireccion(DireccionModField.getText());
            c.setActividadComercial(ActComercModField.getText());
            c.setNombreComercial(ComercialModField.getText());
            c.setRazonSocial(RazonModField.getText());
           
            tablaClientesJuridico.getSelectionModel().clearSelection();
            
        }        
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        con.desconectar();
        tablaClientes.refresh();
        tablaClientesJuridico.refresh();
    }
    //elimina cliente de la base de datos y de la tabla
    @FXML
    private void eliminarCliente(ActionEvent event) throws Exception {
        con.conectar();

        if(tabCliente.isSelected()){
            pst = con.getCon().prepareStatement("start transaction;");
            pst.execute();
            Cliente c = tablaClientes.getSelectionModel().getSelectedItem();
            pst = con.getCon().prepareStatement("select * from contrato where Cliente_Cedula = ?; ");
            pst.setString(1, c.getCedula());
            rst = pst.executeQuery();
            Contrato conSelect = new Contrato();
            while(rst.next()){
                for (Iterator<Contrato> it = Bases.contrato.iterator(); it.hasNext();) {
                    Contrato contrat = it.next();
                    if(rst.getString(4).equalsIgnoreCase(contrat.getCliente().getCedula())){
                        conSelect = contrat;
                    }
                }
            }
            System.out.println(conSelect.getIdContrato());
            try{
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            pst = con.getCon().prepareStatement("commit;");
            Bases.contrato.remove(conSelect);
            System.out.println(c);
            }catch(NullPointerException ex){
                System.out.println(ex);
            }
            con.conectar();
            pst = con.getCon().prepareStatement("Delete from clientenatural where cedula = ?");
            pst.setString(1, c.getCedula());
            pst.execute();
            pst = con.getCon().prepareStatement("Delete from cliente where cedula = ?");
            pst.setString(1, c.getCedula());
            pst.execute();
            pst = con.getCon().prepareStatement("Delete from persona where cedula = ?");
            pst.setString(1, c.getCedula());
            pst.execute();
            clientes.remove((ClienteNatural)c);

            
        }
        if(tabClienteJ.isSelected()){
            Cliente c = tablaClientesJuridico.getSelectionModel().getSelectedItem();
            pst = con.getCon().prepareStatement("select * from contrato where Cliente_Cedula = ?; ");
            pst.setString(1, c.getCedula());
            rst = pst.executeQuery();
            Contrato conSelect = new Contrato();
            try{
            while(rst.next()){
                for (Iterator<Contrato> it = Bases.contrato.iterator(); it.hasNext();) {
                    Contrato contrat = it.next();
                    System.out.println(contrat);
                    if(rst.getString(4).equalsIgnoreCase(contrat.getCliente().getCedula())){
                        conSelect = contrat;
                    }
                }
            } 
            System.out.println(conSelect.getCliente());
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            pst = con.getCon().prepareStatement("commit;");
            Bases.contrato.remove(conSelect);
            System.out.println(c);
            }catch(NullPointerException ex){
                System.out.println(ex);
            }
            pst = con.getCon().prepareStatement("Delete from clientejuridico where cedula = ?");
            pst.setString(1, c.getCedula());
            pst.execute();
            pst = con.getCon().prepareStatement("Delete from cliente where cedula = ?");
            pst.setString(1, c.getCedula());
            pst.execute();
            pst = con.getCon().prepareStatement("Delete from persona where cedula = ?");
            pst.setString(1, c.getCedula());
            pst.execute();
            clientesJ.remove((ClienteJuridico)c);

        }
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        con.desconectar();
        System.out.println("Eliminar CLiente");
        clearSelect();
    }
    //obtiene los valores cunado se seleciona en la tabla
    @FXML
    private void getClienteNatural(MouseEvent event) {
        try{
        Cliente c = tablaClientes.getSelectionModel().getSelectedItem();
        CedulaModField.setText(c.getCedula());
        NombreModField.setText(c.getNombre());
        ApellidoModField.setText(c.getApellido());
        TelefonoModField.setText(c.getTelefono());
        DireccionModField.setText(c.getDireccion());
        ComercialModField.setText(c.getNombreComercial());
        ActComercModField.setText(c.getActividadComercial());
        RazonModField.clear();
        } catch(NullPointerException ex){
            System.out.println(ex);
        }

        
    }
    //obtiene los valores cunado se seleciona en la tabla
    @FXML
    private void getClienteJuridico(MouseEvent event) {
        try{
        ClienteJuridico c = tablaClientesJuridico.getSelectionModel().getSelectedItem();
        CedulaModField.setText(c.getCedula());
        NombreModField.setText(c.getNombre());
        ApellidoModField.setText(c.getApellido());
        TelefonoModField.setText(c.getTelefono());
        DireccionModField.setText(c.getDireccion());
        ComercialModField.setText(c.getNombreComercial());
        ActComercModField.setText(c.getActividadComercial());
        RazonModField.setText(c.getRazonSocial());
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
    }
    //las tres funciones deselacionan para que no haya errores
    @FXML
    private void ClearSelecionCliente(Event event){
        clearSelect();
    }

    @FXML
    private void ClearSelecionJuridico(Event event) {
        clearSelect();
    }

    @FXML
    private void ClearSelecion(MouseEvent event) {
        clearSelect();
    }
    private void clearSelect(){
                try{
        tablaClientes.getSelectionModel().clearSelection();
            tablaClientesJuridico.getSelectionModel().clearSelection();
        CedulaModField.clear();
        NombreModField.clear();
        ApellidoModField.clear();
        TelefonoModField.clear();
        DireccionModField.clear();
        ComercialModField.clear();
        ActComercModField.clear();
        RazonModField.clear();}
        catch(NullPointerException ex) {System.out.println(ex);}
    }
    public void ActivarBoton(){
        nuevoCliente.setDisable(false);
    }
    public void DesactivarBoton(){
        nuevoCliente.setDisable(true);
    }
    private void cargarFiltros(){
        FilteredList<ClienteNatural> filteredNatural = new FilteredList<>(clientes, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredNatural.setPredicate(cliente -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (cliente.getCedula().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (cliente.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (cliente.getApellido().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }                
                  else if (cliente.getTelefono().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getDireccion().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getNombreComercial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getActividadComercial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                return false; // Does not match.
            });
        });
        tablaClientes.setItems(filteredNatural);
        FilteredList<ClienteJuridico> filteredJuridico = new FilteredList<>(clientesJ, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredJuridico.setPredicate(cliente -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (cliente.getCedula().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (cliente.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (cliente.getApellido().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }                
                  else if (cliente.getTelefono().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getDireccion().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getNombreComercial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getActividadComercial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (cliente.getRazonSocial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }                   
                return false; // Does not match.
            });
        });
        tablaClientesJuridico.setItems(filteredJuridico);
    
    }


}
