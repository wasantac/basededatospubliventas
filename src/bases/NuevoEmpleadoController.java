/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Cargo;
import basesObjetos.Conexion;
import basesObjetos.Empleado;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class NuevoEmpleadoController implements Initializable {

    @FXML
    private VBox window;
    @FXML
    private TextField cedulaField;
    @FXML
    private TextField nombreField;
    @FXML
    private TextField apellidoField;
    @FXML
    private TextField telefonoField;
    @FXML
    private TextArea direccionField;
    @FXML
    private TextField emailField;
    @FXML
    private RadioButton disenadorRD;
    @FXML
    private ToggleGroup tipo;
    @FXML
    private RadioButton vendedorRD;
    @FXML
    private Button crearbtn;
    @FXML
    private Button cancelarbtn;
    private Conexion con = new Conexion();
    private PreparedStatement pst;
    private ResultSet rst;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        disenadorRD.fire();
    }    
    //permite crear un empleado y agregarlo a la base de datos
    @FXML
    private void CrearEmpleado(ActionEvent event) throws Exception {
        String cedula = cedulaField.getText();
        String nombre = nombreField.getText();
        String apellido = apellidoField.getText();
        String telefono = telefonoField.getText();
        String direccion = direccionField.getText();
        String email = emailField.getText();
        boolean confirmacion = checkearCedula(cedula);
        if(confirmacion = true){
            con.conectar();
            if(disenadorRD.isSelected()){
                int cargo = Cargo.DISENADOR.getId();
                
                System.out.println("Es cliente Disenador");
                String query  = "insert into persona(cedula,nombre,apellido,telefono,direccion) values(?,?,?,?,?);";
                String query2  = "insert into empleado(Cedula,email,Cargo_idCargo) values(?,?,?);";


                pst = con.getCon().prepareStatement(query);
                pst.setString(1, cedula);
                pst.setString(2, nombre);
                pst.setString(3, apellido);
                pst.setString(4, telefono);
                pst.setString(5, direccion);

                pst.execute();
                
                pst = con.getCon().prepareStatement(query2);
                pst.setString(1, cedula);
                pst.setString(2, email);
                pst.setString(3,Integer.toString(cargo));
                
                pst.execute();
                

                Empleado e = new Empleado(cedula,nombre,apellido,telefono,direccion,email,Cargo.DISENADOR.getSueldo());
                EmpleadosController.disenador.add(e); 
            }
            if(vendedorRD.isSelected()){
                int cargo = Cargo.VENDEDOR.getId();
                
                System.out.println("Es cliente Vendedor");
                String query  = "insert into persona(cedula,nombre,apellido,telefono,direccion) values(?,?,?,?,?);";
                String query2  = "insert into empleado(Cedula,email,Cargo_idCargo) values(?,?,?);";


                pst = con.getCon().prepareStatement(query);
                pst.setString(1, cedula);
                pst.setString(2, nombre);
                pst.setString(3, apellido);
                pst.setString(4, telefono);
                pst.setString(5, direccion);

                pst.execute();
                
                pst = con.getCon().prepareStatement(query2);
                pst.setString(1, cedula);
                pst.setString(2, email);
                pst.setString(3,Integer.toString(cargo));
                
                pst.execute();
                

                Empleado e = new Empleado(cedula,nombre,apellido,telefono,direccion,email,Cargo.VENDEDOR.getSueldo());
                EmpleadosController.vendedor.add(e); 
                
            }
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.close();
            
        }
        
    }
    //cierra la ventana
    @FXML
    private void cancelar(ActionEvent event) {
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }
    private boolean checkearCedula(String cedula){
        return cedula.length() == 10;
    }
    
}
