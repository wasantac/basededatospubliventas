/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Cliente;
import basesObjetos.ClienteNatural;
import basesObjetos.ClienteJuridico;
import basesObjetos.Conexion;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class NuevoClienteController implements Initializable {

    @FXML
    private TextField cedulaField;
    @FXML
    private TextField nombreField;
    @FXML
    private TextField apellidoField;
    @FXML
    private TextField telefonoField;
    @FXML
    private TextField comercialField;
    @FXML
    private TextArea direccionField;
    @FXML
    private TextField actividadField;
    @FXML
    private TextField socialField;
    @FXML
    private RadioButton naturalRD;
    @FXML
    private ToggleGroup tipo;
    @FXML
    private RadioButton juridicoRD;
    @FXML
    private Button crearbtn;
    @FXML
    private Button cancelarbtn;
    
    private Conexion con = new Conexion();
    private PreparedStatement pst;
    private ResultSet rst;

    @FXML
    private VBox window;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //activa el boton si todos los datos estan completos
        crearbtn.disableProperty().bind(Bindings.isEmpty(cedulaField.textProperty())
                                        .or(Bindings.isEmpty(apellidoField.textProperty()))
                                        .or(Bindings.isEmpty(telefonoField.textProperty()))
                                        .or(Bindings.isEmpty(comercialField.textProperty()))
                                        .or(Bindings.isEmpty(direccionField.textProperty()))
                                        .or(Bindings.isEmpty(actividadField.textProperty())));
        naturalRD.fire();

    }    

    //cierra la ventana
    @FXML
    private void cancelar(ActionEvent event) throws IOException {
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }
    //permite crear un nuevo cliente y lo agrega a la base de datos
    @FXML
    private void CrearCliente(ActionEvent event) throws Exception {

        String cedula = cedulaField.getText();
        String nombre = nombreField.getText();
        String apellido = apellidoField.getText();
        String telefono = telefonoField.getText();
        String comercial = comercialField.getText();
        String direccion = direccionField.getText();
        String actividad = actividadField.getText();
        String social = socialField.getText();
        boolean confirmacion = checkearCedula(cedula);
        if(confirmacion = true){
            con.conectar();
            if(juridicoRD.isSelected()){
                System.out.println("Es cliente Juridico");
                String query  = "insert into persona(cedula,nombre,apellido,telefono,direccion) values(?,?,?,?,?);";
                String query2  = "insert into cliente(cedula,actividadComercial,nombreComercial) values(?,?,?);";
                String query3  = "insert into clientejuridico(cedula,razonSocia) values(?,?);"; 

                pst = con.getCon().prepareStatement(query);
                pst.setString(1, cedula);
                pst.setString(2, nombre);
                pst.setString(3, apellido);
                pst.setString(4, telefono);
                pst.setString(5, direccion);

                pst.execute();
                
                pst = con.getCon().prepareStatement(query2);
                pst.setString(1, cedula);
                pst.setString(2, actividad);
                pst.setString(3, comercial);
                
                pst.execute();
                
                pst = con.getCon().prepareStatement(query3);
                pst.setString(1, cedula);
                pst.setString(2, social);
                
                pst.execute();
                ClienteJuridico c = new ClienteJuridico(cedula,nombre,apellido,telefono,direccion,actividad,comercial,social);
                ClientesController.clientesJ.add(c);
            

            }
            if(naturalRD.isSelected()){
                System.out.println("Es cliente natural");
                String query  = "insert into persona(cedula,nombre,apellido,telefono,direccion) values(?,?,?,?,?);";
                String query2  = "insert into cliente(cedula,actividadComercial,nombreComercial) values(?,?,?);";
                String query3  = "insert into clientenatural(cedula) values(?);"; 

                pst = con.getCon().prepareStatement(query);
                pst.setString(1, cedula);
                pst.setString(2, nombre);
                pst.setString(3, apellido);
                pst.setString(4, telefono);
                pst.setString(5, direccion);

                pst.execute();
                
                pst = con.getCon().prepareStatement(query2);
                pst.setString(1, cedula);
                pst.setString(2, actividad);
                pst.setString(3, comercial);
                
                pst.execute();
                
                pst = con.getCon().prepareStatement(query3);
                pst.setString(1, cedula);
                
                pst.execute();

                
                ClienteNatural c = new ClienteNatural(cedula,nombre,apellido,telefono,direccion,actividad,comercial);
                ClientesController.clientes.add(c);

                


            }

                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.close();
                con.desconectar();

        }
        
    }
    
    private boolean checkearCedula(String cedula){
       if(cedula.length() == 10){
           return true;
       }
       else{
           return false;
       }
    }
    
    @FXML
    private void desactivarSocial(ActionEvent event) {
        System.out.println("Cliente Natural");
        socialField.setEditable(false);
        socialField.clear();
        socialField.setStyle("-fx-background-color: gray");      



    }

    @FXML
    private void activarSocial(ActionEvent event) {
        try{
        System.out.println("ClienteJuridico");
        socialField.setEditable(true);
        socialField.setStyle("-fx-background-color: white");  
        } catch(NullPointerException ex){
            System.out.println(ex);
        }
    }

    
    
    }
