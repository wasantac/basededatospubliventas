/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import static bases.ClientesController.clientes;
import static bases.EmpleadosController.disenador;
import basesObjetos.Cliente;
import basesObjetos.ClienteNatural;
import basesObjetos.Conexion;
import basesObjetos.Contrato;
import basesObjetos.Detalle;
import basesObjetos.Empleado;
import basesObjetos.Fisico;
import basesObjetos.Material;
import basesObjetos.Pago;
import basesObjetos.Paquete;
import basesObjetos.Producto;
import basesObjetos.Servicio;
import basesObjetos.Virtual;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class ProductosController implements Initializable {

    @FXML
    private BorderPane pantalla;
    @FXML
    private Button atrasBtn;
    @FXML
    private Label titulo;
    @FXML
    private TextField buscarField;
    @FXML
    private TabPane Tablastab;
    @FXML
    private Tab virtualTab;
    @FXML
    private TableColumn<Virtual, String> idProductoV;
    @FXML
    private TableColumn<Virtual, String> nombreV;
    @FXML
    private TableColumn<Virtual, String> descripcionV;
    @FXML
    private TableColumn<Virtual, String> precioV;
    @FXML
    private TableColumn<Virtual, String> tiempoV;
    @FXML
    private TableColumn<Virtual, String> tipoV;
    @FXML
    private TableColumn<Fisico, String> idProductoF;
    @FXML
    private TableColumn<Fisico, String> nombreF;
    @FXML
    private TableColumn<Fisico, String> descripcionF;
    @FXML
    private TableColumn<Fisico, String> precioF;
    @FXML
    private TableColumn<Fisico, String> tiempoF;
    @FXML
    private TableColumn<Fisico, String> tipoF;
    @FXML
    private Tab serviciosTab;
    @FXML
    private TableColumn<Servicio, String> idProductoS;
    @FXML
    private TableColumn<Servicio, String> nombreS;
    @FXML
    private TableColumn<Servicio, String> descripcionS;
    @FXML
    private TableColumn<Servicio, String> precioS;
    @FXML
    private TableColumn<Servicio, String> tiempoS;
    @FXML
    private TableColumn<Servicio, String> tipoS;
    @FXML
    private TableColumn<Material, String> idMaterial;
    @FXML
    private TableColumn<Material, String> nombreM;
    @FXML
    private GridPane gridFields;
    @FXML
    private Button nuevoPbtn;
    @FXML
    private Button modificarBtn;
    @FXML
    private Button EliminarBtn;
    public static ObservableList<Virtual> productoV = Bases.productoV;
    public static ObservableList<Fisico> productoF = Bases.productoF;
    public static ObservableList<Servicio> productoS = Bases.productoS;
    public static ObservableList<Material> material = Bases.material;
    @FXML
    private TableView<Virtual> TablaProductoV;
    @FXML
    private TableView<Fisico> TablaProductoF;
    @FXML
    private TableView<Servicio> TablaProductoS;
    @FXML
    private TableView<Material> TablaMaterial;
    @FXML
    private TableColumn<Material,Double> precioM;
    @FXML
    private TableColumn<Material, Material> materialF;
    @FXML
    private TableColumn<Fisico, String> ancho;
    @FXML
    private TableColumn<Fisico, String> alto;
    @FXML
    private TextField anchoField;
    @FXML
    private TextField altoField;
    @FXML
    private TextField nombreField;
    @FXML
    private TextField tipoField;
    @FXML
    private TextField descripcionField;
    @FXML
    private TextField precioField;
    @FXML
    private Tab fisicoTab;
    @FXML
    private Tab materialTab;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //permite hacer busquedas de lo que se requiera en la barra de cualquier dato
        setCellTableProductoVirtual();
        setCellTableProductoFisico();
        setCellTableProductoServicio();
        setCellTableMaterial();
                FilteredList<Material> filteredMaterial = new FilteredList<>(material, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredMaterial.setPredicate(material -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (material.getNombreMaterial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(material.getIdMaterial()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (Double.toString(material.getPrecio()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }                  
                return false; // Does not match.
            });
        });
                FilteredList<Virtual> filteredVirtual = new FilteredList<>(productoV, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredVirtual.setPredicate(virtual -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (virtual.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(virtual.getIdProducto()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (Double.toString(virtual.getPrecio()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (virtual.getTiempoEmpleado().toString().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (virtual.getTipo().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (virtual.getDescripcion().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
            });
        });
                FilteredList<Fisico> filteredFisico = new FilteredList<>(productoF, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredFisico.setPredicate(fisico -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (fisico.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(fisico.getIdProducto()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                 else if (Integer.toString(fisico.getAlto()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                 else if (Integer.toString(fisico.getAncho()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (Double.toString(fisico.getPrecio()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (fisico.getMaterial().getNombreMaterial().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (fisico.getTiempoEmpleado().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (fisico.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (fisico.getTipo().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
            });
        });
                FilteredList<Servicio> filteredServicio = new FilteredList<>(productoS, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredServicio.setPredicate(servicio -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (servicio.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (Integer.toString(servicio.getIdProducto()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (Double.toString(servicio.getPrecio()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (servicio.getDescripcion().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (servicio.getTipo().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                else if (servicio.getTiempoEmpleado().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                return false; // Does not match.
            });
        });
        TablaProductoV.setItems(filteredVirtual);
        TablaProductoF.setItems(filteredFisico);
        TablaMaterial.setItems(filteredMaterial);
        TablaProductoS.setItems(filteredServicio);
        
    }    

    @FXML
    private void atras(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
    private void setCellTableProductoVirtual(){
        this.idProductoV.setCellValueFactory(new PropertyValueFactory("idProducto"));
        this.nombreV.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.descripcionV.setCellValueFactory(new PropertyValueFactory("Descripcion"));
        this.precioV.setCellValueFactory(new PropertyValueFactory("Precio"));
        this.tiempoV.setCellValueFactory(new PropertyValueFactory("TiempoEmpleado"));
        this.tipoV.setCellValueFactory(new PropertyValueFactory("Tipo"));
    //carga los datos de la base de datos y los carga en la tabla    
    }
    private void setCellTableProductoFisico(){
        this.idProductoF.setCellValueFactory(new PropertyValueFactory("idProducto"));
        this.nombreF.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.descripcionF.setCellValueFactory(new PropertyValueFactory("Descripcion"));
        this.precioF.setCellValueFactory(new PropertyValueFactory("Precio"));
        this.tiempoF.setCellValueFactory(new PropertyValueFactory("TiempoEmpleado"));
        this.tipoF.setCellValueFactory(new PropertyValueFactory("Tipo"));
        this.ancho.setCellValueFactory(new PropertyValueFactory("Ancho"));
        this.alto.setCellValueFactory(new PropertyValueFactory("Alto"));
        this.materialF.setCellValueFactory(new PropertyValueFactory("Material"));
    //carga los datos de la base de datos y los carga en la tabla    
    }
    private void setCellTableProductoServicio(){
        this.idProductoS.setCellValueFactory(new PropertyValueFactory("idProducto"));
        this.nombreS.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.descripcionS.setCellValueFactory(new PropertyValueFactory("Descripcion"));
        this.precioS.setCellValueFactory(new PropertyValueFactory("Precio"));
        this.tiempoS.setCellValueFactory(new PropertyValueFactory("TiempoEmpleado"));
        this.tipoS.setCellValueFactory(new PropertyValueFactory("Tipo"));
        
        
    //carga los datos de la base de datos y los carga en la tabla    
    }
    private void setCellTableMaterial(){
        this.idMaterial.setCellValueFactory(new PropertyValueFactory("idMaterial"));
        this.nombreM.setCellValueFactory(new PropertyValueFactory("nombreMaterial"));
        this.precioM.setCellValueFactory(new PropertyValueFactory("precio"));

    //carga los datos de la base de datos y los carga en la tabla    
    }
    //permite crear un producto
    @FXML
    private void crearProducto(ActionEvent event) throws IOException {
        clearFields();
        Parent root = FXMLLoader.load(getClass().getResource("NuevoProducto.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("Nuevo Producto");
        stage.show();
        Thread thread = new Thread(){
          public void run(){
            System.out.println("Thread Running");
            while(stage.isShowing()){
                DesactivarBoton();
            }
            ActivarBoton();
          }
          
        };
        thread.start();
    }

    @FXML
    private void modificar(ActionEvent event) throws SQLException, Exception {
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        pst.execute();
        if(virtualTab.isSelected()){
            Virtual v = TablaProductoV.getSelectionModel().getSelectedItem();
            pst = con.getCon().prepareStatement("call actualizarProducto(?,?,?,?)");
            pst.setInt(1, v.getIdProducto());
            pst.setString(2, nombreField.getText());
            pst.setString(3, descripcionField.getText());
            pst.setDouble(4, Double.parseDouble(precioField.getText()));
            pst.execute();
            v.setNombre(nombreField.getText());
            v.setDescripcion(descripcionField.getText());
            v.setPrecio(Double.parseDouble(precioField.getText()));
            TablaProductoV.refresh();
        }
        if(serviciosTab.isSelected()){
            Servicio v = TablaProductoS.getSelectionModel().getSelectedItem();
            pst = con.getCon().prepareStatement("call actualizarProducto(?,?,?,?)");
            pst.setInt(1, v.getIdProducto());
            pst.setString(2, nombreField.getText());
            pst.setString(3, descripcionField.getText());
            pst.setDouble(4, Double.parseDouble(precioField.getText()));
            pst.execute();
            v.setNombre(nombreField.getText());
            v.setDescripcion(descripcionField.getText());
            v.setPrecio(Double.parseDouble(precioField.getText()));
            TablaProductoS.refresh();
        }
        if(fisicoTab.isSelected()){
            Fisico v = TablaProductoF.getSelectionModel().getSelectedItem();
            pst = con.getCon().prepareStatement("call actualizarProducto(?,?,?,?)");
            pst.setInt(1, v.getIdProducto());
            pst.setString(2, nombreField.getText());
            pst.setString(3, descripcionField.getText());
            pst.setDouble(4, Double.parseDouble(precioField.getText()));
            pst.execute();
            v.setNombre(nombreField.getText());
            v.setDescripcion(descripcionField.getText());
            v.setPrecio(Double.parseDouble(precioField.getText()));
            TablaProductoF.refresh();
            
        }
        if(materialTab.isSelected()){
            Material m = TablaMaterial.getSelectionModel().getSelectedItem();
            pst = con.getCon().prepareStatement("call actualizarMaterial(?,?,?)");
            pst.setInt(1, m.getIdMaterial());
            pst.setString(2, nombreField.getText());
            pst.setDouble(3, Double.parseDouble(precioField.getText()));
            pst.execute();
            m.setNombreMaterial(nombreField.getText());
            m.setPrecio(Double.parseDouble(precioField.getText()));
            TablaProductoF.refresh();
            TablaMaterial.refresh();
            
        }
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        clearFields();
    }

    @FXML
    private void eliminar(ActionEvent event) throws Exception {
        
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        pst.execute();
        if(virtualTab.isSelected()){
            Contrato conSelect = new Contrato();
            Virtual v = TablaProductoV.getSelectionModel().getSelectedItem();
            try{
            for(Contrato c: Bases.contrato){
                for(Detalle d: c.getDetalles()){
                    if(d.getPaquete().getVirtual().equals(v)){
                        conSelect = c;
                    }
                }
            }
            System.out.println(conSelect.getIdContrato());
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            
            Bases.contrato.remove(conSelect);
            }catch(NullPointerException ex){
                System.out.println(ex);
            }
            System.out.println(v);
            pst = con.getCon().prepareStatement("delete from paquete where Virtual_idProducto = ?;");
            pst.setInt(1, v.getIdProducto());
            pst.execute();     
            pst = con.getCon().prepareStatement("delete from prodvirtual where idProducto = ?;");
            pst.setInt(1, v.getIdProducto());
            pst.execute();
            pst = con.getCon().prepareStatement("delete from producto where idProducto = ?");
            pst.setInt(1, v.getIdProducto());
            pst.execute();
            productoV.remove(v);
        }
        if(serviciosTab.isSelected()){
            Contrato conSelect = new Contrato();
            Servicio s = TablaProductoS.getSelectionModel().getSelectedItem();
            try{

            for(Contrato c: Bases.contrato){
                for(Detalle d: c.getDetalles()){
                    if(d.getPaquete().getServicio().equals(s)){
                        conSelect = c;
                    }
                }
            }
            System.out.println(conSelect.getIdContrato());
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Bases.contrato.remove(conSelect);
            }catch(NullPointerException ex){
                System.out.println(ex);
            }
            System.out.println(s);
            pst = con.getCon().prepareStatement("delete from paquete where Servicio_idProducto = ?;");
            pst.setInt(1, s.getIdProducto());
            pst.execute();      
            pst = con.getCon().prepareStatement("delete from servicio where idProducto = ?;");
            pst.setInt(1, s.getIdProducto());
            pst.execute();
            pst = con.getCon().prepareStatement("delete from producto where idProducto = ?");
            pst.setInt(1, s.getIdProducto());
            pst.execute();
            productoS.remove(s);
        
        }
        if(fisicoTab.isSelected()){
            Contrato conSelect = new Contrato();
            Fisico f = TablaProductoF.getSelectionModel().getSelectedItem();
            try{
            for(Contrato c: Bases.contrato){
                for(Detalle d: c.getDetalles()){
                    if(d.getPaquete().getFisico().equals(f)){
                        conSelect = c;
                    }
                }
            }
            System.out.println(conSelect.getIdContrato());
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Bases.contrato.remove(conSelect);
            } catch(NullPointerException ex){
                System.out.println(ex);
            }
            System.out.println(f);
            con.conectar();
            pst = con.getCon().prepareStatement("delete from paquete where Fisico_idProducto = ?;");
            pst.setInt(1, f.getIdProducto());
            pst.execute();   
            pst = con.getCon().prepareStatement("delete from fisico where idProducto = ?;");
            pst.setInt(1, f.getIdProducto());
            pst.execute();
            pst = con.getCon().prepareStatement("delete from producto where idProducto = ?");
            pst.setInt(1, f.getIdProducto());
            pst.execute();
            productoF.remove(f);
        
        }
        if(materialTab.isSelected()){
            Contrato conSelect = new Contrato();
            Material m = TablaMaterial.getSelectionModel().getSelectedItem();
            try{

                for(Contrato c: Bases.contrato){
                    for(Detalle d: c.getDetalles()){
                        System.out.println("aqi");
                        if(d.getPaquete().getFisico().getMaterial().equals(m)){
                            
                            conSelect = c;
                        }
                    }
                }
                System.out.println(conSelect.getIdContrato());
                for(Detalle d : conSelect.getDetalles()){
                    pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                    pst.setInt(1, conSelect.getIdContrato());
                    pst.execute();


                }
                for(Detalle d : conSelect.getDetalles()){
                    int idP = d.getPaquete().getIdPaquete();
                    System.out.println(idP);
                    pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                    pst.setInt(1, idP);
                    pst.execute();
                    Paquete paquete = new Paquete();
                    for(Paquete p : Bases.paquete){
                        if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                            paquete = p;
                        }
                    }
                    Bases.paquete.remove(paquete);
                }

                pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();
                Pago pago = new Pago();
                for(Pago p : Bases.pago){
                    if(p.getIdContrato() == conSelect.getIdContrato()){
                        pago = p;
                    }
                }
                Bases.pago.remove(pago);
                pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();
                Bases.contrato.remove(conSelect);
                    }catch(NullPointerException ex){
                System.out.println(ex);
                }
                System.out.println(m);
                ObservableList<Fisico> eliminar = FXCollections.observableArrayList();
                for(Fisico f: productoF){
                    if(f.getMaterial().getIdMaterial() == m.getIdMaterial()){
                        pst = con.getCon().prepareStatement("delete from paquete where Fisico_idProducto = ?;");
                        pst.setInt(1, f.getIdProducto());
                        pst.execute();   
                        pst = con.getCon().prepareStatement("delete from fisico where idMaterial = ?;");
                        pst.setInt(1, m.getIdMaterial());
                        pst.execute();
                        pst = con.getCon().prepareStatement("delete from producto where idProducto = ?");
                        pst.setInt(1, f.getIdProducto());
                        pst.execute();
                        eliminar.add(f);
                    }
                }
                productoF.removeAll(eliminar);
                pst = con.getCon().prepareStatement("delete from material where idMaterial= ?;");
                pst.setInt(1, m.getIdMaterial());
                pst.execute();

                material.remove(m);    

        }
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        con.desconectar();
        clearFields();
    }
    
    public void ActivarBoton(){
        nuevoPbtn.setDisable(false);
    }
    public void DesactivarBoton(){
        nuevoPbtn.setDisable(true);
    }
    //abre una ventana para crear un material
    @FXML
    private void nuevoMaterial(ActionEvent event) throws IOException {
        clearFields();
        Parent root = FXMLLoader.load(getClass().getResource("NuevoMaterial.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("Nuevo Material");
        stage.show();
        Thread thread = new Thread(){
          public void run(){
            System.out.println("Thread Running");
            while(stage.isShowing()){
                DesactivarBoton();
            }
            ActivarBoton();
          }
          
        };
        thread.start();
    }

    @FXML
    private void getVirtual(MouseEvent event) {
        clearFields();
        try{
        Virtual v = TablaProductoV.getSelectionModel().getSelectedItem();
        nombreField.setText(v.getNombre());
        descripcionField.setText(v.getDescripcion());
        tipoField.setText(v.getTipo());
        precioField.setText(Double.toString(v.getPrecio()));
        } catch(NullPointerException ex){
            System.out.println(ex);
        }
        
    }

    @FXML
    private void getFisico(MouseEvent event) {
        clearFields();
        try{
        Fisico f = TablaProductoF.getSelectionModel().getSelectedItem();
        nombreField.setText(f.getNombre());
        descripcionField.setText(f.getDescripcion());
        tipoField.setText(f.getTipo());
        precioField.setText(Double.toString(f.getPrecio()));
        anchoField.setText(Integer.toString(f.getAncho()));
        altoField.setText(Integer.toString(f.getAlto()));
        } catch(NullPointerException ex){
            System.out.println(ex);
        }
    }

    @FXML
    private void getServicio(MouseEvent event) {
        clearFields();
        try{
        Servicio s = TablaProductoS.getSelectionModel().getSelectedItem();
        nombreField.setText(s.getNombre());
        descripcionField.setText(s.getDescripcion());
        tipoField.setText(s.getTipo());
        precioField.setText(Double.toString(s.getPrecio()));
        } catch(NullPointerException ex){
            System.out.println(ex);
        }

    }

    @FXML
    private void getMaterial(MouseEvent event) {
        clearFields();
        try{
        Material m = TablaMaterial.getSelectionModel().getSelectedItem();
        nombreField.setText(m.getNombreMaterial());
        precioField.setText(Double.toString(m.getPrecio()));

        } catch(NullPointerException ex){
            System.out.println(ex);
        }
    }
    @FXML
    private void clearFields(){
        nombreField.clear();
        descripcionField.clear();
        tipoField.clear();
        precioField.clear();
        anchoField.clear();
        altoField.clear();
        
    }
}
