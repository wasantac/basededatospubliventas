/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import static bases.ClientesController.clientes;
import basesObjetos.Cargo;
import basesObjetos.Cliente;
import basesObjetos.ClienteJuridico;
import basesObjetos.ClienteNatural;
import basesObjetos.Conexion;
import basesObjetos.Contrato;
import basesObjetos.Detalle;
import basesObjetos.Empleado;
import basesObjetos.Pago;
import basesObjetos.Paquete;
import basesObjetos.Persona;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class EmpleadosController implements Initializable {

    @FXML
    private Button atras;
    @FXML
    private TextField buscarField;
    @FXML
    private TableColumn<Empleado, String> cedula;
    @FXML
    private TableColumn<Empleado, String> nombre;
    @FXML
    private TableColumn<Empleado, String> apellido;
    @FXML
    private TableColumn<Empleado, String> sueldo;
    @FXML
    private TableColumn<Empleado, String> telefono;
    @FXML
    private Button nuevoEmpleado;
    @FXML
    private Button Modificar;
    @FXML
    private Button Eliminar;
    @FXML
    private TableColumn<Empleado, String> email;
    @FXML
    private Tab disenadorTab;
    @FXML
    private Tab vendedorTab;
    @FXML
    private TableColumn<Empleado, String> cedulaV;
    @FXML
    private TableColumn<Empleado, String> nombreV;
    @FXML
    private TableColumn<Empleado, String> apellidoV;
    @FXML
    private TableColumn<Empleado, String> telefonoV;
    @FXML
    private TableColumn<Empleado, String> emailV;
    @FXML
    private TableColumn<Empleado, String> sueldoV;
    @FXML
    private TableView<Empleado> tablaDisenador;
    @FXML
    private TableView<Empleado> tablaVendedor;
    @FXML
    private TableColumn<Empleado, String> direccion;
    @FXML
    private TableColumn<Empleado, String> direccionV;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    public static ObservableList<Empleado> disenador = Bases.disenador;
    public static ObservableList<Empleado> vendedor = Bases.vendedor;
    private ObservableList<Persona> personas = FXCollections.observableArrayList();
    @FXML
    private TextField cedulaField;
    @FXML
    private TextField direccionField;
    @FXML
    private TextField telefonoField;
    @FXML
    private TextField nombreField;
    @FXML
    private TextField apellidoField;
    @FXML
    private TextField emailField;
    @FXML
    private BorderPane fondo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setCellTable(); //inicializan las tablas
        setCellTable2();
        //sirve para hacer busquedas y que salgan los datos automaticamente
        FilteredList<Empleado> filteredDisenador = new FilteredList<>(disenador, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredDisenador.setPredicate(empleado -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (empleado.getCedula().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (empleado.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (empleado.getApellido().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }                
                  else if (empleado.getTelefono().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (empleado.getDireccion().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (empleado.getEmail().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                return false; // Does not match.
            });
        });
        FilteredList<Empleado> filteredVendedor = new FilteredList<>(vendedor, p -> true);
        buscarField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredVendedor.setPredicate(empleado -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                
                if (empleado.getCedula().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (empleado.getNombre().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
                  else if (empleado.getApellido().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }                
                  else if (empleado.getTelefono().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (empleado.getDireccion().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                  else if (empleado.getEmail().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }    
                return false; // Does not match.
            });
        });
        
        tablaDisenador.setItems(filteredDisenador);
        tablaVendedor.setItems(filteredVendedor);
    }    
    //regresa a la panatalla principal
    @FXML
    private void changeScreen(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
        clearSelect();
    }
    private void setCellTable(){
        this.cedula.setCellValueFactory(new PropertyValueFactory("Cedula"));
        this.nombre.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.apellido.setCellValueFactory(new PropertyValueFactory("Apellido"));
        this.telefono.setCellValueFactory(new PropertyValueFactory("Telefono"));
        this.direccion.setCellValueFactory(new PropertyValueFactory("Direccion"));
        this.email.setCellValueFactory(new PropertyValueFactory("Email"));
        this.sueldo.setCellValueFactory(new PropertyValueFactory("Sueldo"));
    //carga los datos de la base de datos y los carga en la tabla    
    }
    private void setCellTable2(){
        this.cedulaV.setCellValueFactory(new PropertyValueFactory("Cedula"));
        this.nombreV.setCellValueFactory(new PropertyValueFactory("Nombre"));
        this.apellidoV.setCellValueFactory(new PropertyValueFactory("Apellido"));
        this.telefonoV.setCellValueFactory(new PropertyValueFactory("Telefono"));
        this.direccionV.setCellValueFactory(new PropertyValueFactory("Direccion"));
        this.emailV.setCellValueFactory(new PropertyValueFactory("Email"));
        this.sueldoV.setCellValueFactory(new PropertyValueFactory("Sueldo"));
    //carga los datos de la base de datos y los carga en la tabla    
    }
    //abre una nueva ventana para crear un empleado
    @FXML
    private void crearEmpleado(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("NuevoEmpleado.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("Nuevo Empleado");
        stage.show();
        clearSelect();
        Thread thread = new Thread(){
          public void run(){
            System.out.println("Thread Running");
            while(stage.isShowing()){
                DesactivarBoton();
            }
            ActivarBoton();
          }
          
        };
        thread.start();
    }
    //modifica un empleado de la tbla y actualiza con la base de datos
    @FXML
    private void modificarEmpleado(ActionEvent event) throws Exception {
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        pst.execute();
        try{
        if(disenadorTab.isSelected()){
            Empleado e = tablaDisenador.getSelectionModel().getSelectedItem();
            String query  = "call actualizarEmpleado(?,?,?,?,?,?);";
            pst = con.getCon().prepareStatement(query);
            pst.setString(1,cedulaField.getText() );
            pst.setString(2, nombreField.getText());
            pst.setString(3, apellidoField.getText());
            pst.setString(4, telefonoField.getText());
            pst.setString(5, direccionField.getText());
            pst.setString(6, emailField.getText());
            pst.execute();
            e.setNombre(nombreField.getText());
            e.setApellido(apellidoField.getText());
            e.setTelefono(telefonoField.getText());
            e.setDireccion(direccionField.getText());
            e.setEmail(emailField.getText());
            tablaDisenador.refresh();
                
        }
        if(vendedorTab.isSelected()){
            Empleado e = tablaVendedor.getSelectionModel().getSelectedItem();
            String query  = "call actualizarEmpleado(?,?,?,?,?,?);";
            pst = con.getCon().prepareStatement(query);
            pst.setString(1,cedulaField.getText() );
            pst.setString(2, nombreField.getText());
            pst.setString(3, apellidoField.getText());
            pst.setString(4, telefonoField.getText());
            pst.setString(5, direccionField.getText());
            pst.setString(6, emailField.getText());
            pst.execute();
            e.setNombre(nombreField.getText());
            e.setApellido(apellidoField.getText());
            e.setTelefono(telefonoField.getText());
            e.setDireccion(direccionField.getText());
            e.setEmail(emailField.getText());
            tablaVendedor.refresh();
        
        }
        } catch(SQLIntegrityConstraintViolationException ex){
            System.out.println(ex);
        }
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        con.desconectar();
    }
    //elimina un empleado de la tabla y de la base de datos
    @FXML
    private void eliminarEmpleado(ActionEvent event) throws Exception {
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        pst.execute();
        if(disenadorTab.isSelected()){
            Empleado e = tablaDisenador.getSelectionModel().getSelectedItem();
            try{
            pst = con.getCon().prepareStatement("select * from contrato where Empleado_Cedula = ?; ");
            pst.setString(1, e.getCedula());
            rst = pst.executeQuery();
            Contrato conSelect = new Contrato();
            while(rst.next()){
                for (Iterator<Contrato> it = Bases.contrato.iterator(); it.hasNext();) {
                    Contrato contrat = it.next();
                    if(rst.getString(4).equalsIgnoreCase(contrat.getCliente().getCedula())){
                        conSelect = contrat;
                    }
                }
            }
            System.out.println(conSelect.getIdContrato());
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Bases.contrato.remove(conSelect);
            } catch(NullPointerException ex){
                System.out.println(ex);
            }
            System.out.println(e);


            pst = con.getCon().prepareStatement("Delete from empleado where Cedula = ?");
            pst.setString(1, e.getCedula());
            pst.execute();
            pst = con.getCon().prepareStatement("Delete from persona where cedula = ?");
            pst.setString(1, e.getCedula());
            pst.execute();
            disenador.remove((Empleado)e);
        }
        if(vendedorTab.isSelected()){
            Empleado e = tablaVendedor.getSelectionModel().getSelectedItem();
            try{
            pst = con.getCon().prepareStatement("select * from contrato where Empleado_Cedula = ?; ");
            pst.setString(1, e.getCedula());
            rst = pst.executeQuery();
            Contrato conSelect = new Contrato();
            while(rst.next()){
                for (Iterator<Contrato> it = Bases.contrato.iterator(); it.hasNext();) {
                    Contrato contrat = it.next();
                    if(rst.getString(4).equalsIgnoreCase(contrat.getCliente().getCedula())){
                        conSelect = contrat;
                    }
                }
            }
            System.out.println(conSelect.getIdContrato());
            for(Detalle d : conSelect.getDetalles()){
                pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
                pst.setInt(1, conSelect.getIdContrato());
                pst.execute();


            }
            for(Detalle d : conSelect.getDetalles()){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                Paquete paquete = new Paquete();
                for(Paquete p : Bases.paquete){
                    if(p.getIdPaquete() == d.getPaquete().getIdPaquete()){
                        paquete = p;
                    }
                }
                Bases.paquete.remove(paquete);
            }

            pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Pago pago = new Pago();
            for(Pago p : Bases.pago){
                if(p.getIdContrato() == conSelect.getIdContrato()){
                    pago = p;
                }
            }
            Bases.pago.remove(pago);
            pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();
            Bases.contrato.remove(conSelect);
            } catch(NullPointerException ex){
                System.out.println(ex);
            }
            System.out.println(e);
            pst = con.getCon().prepareStatement("Delete from empleado where cedula = ?");
            pst.setString(1, e.getCedula());
            pst.execute();
            pst = con.getCon().prepareStatement("Delete from persona where cedula = ?");
            pst.setString(1, e.getCedula());
            pst.execute();
            vendedor.remove((Empleado)e);
        }
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        con.desconectar();
        clearSelect();
    }
   // las dos funciones nos permiten hacer click y obtener los datos
    @FXML
    private void getDisenador(MouseEvent event) {
        try{
        Empleado e = tablaDisenador.getSelectionModel().getSelectedItem();
        cedulaField.setText(e.getCedula());
        nombreField.setText(e.getNombre());
        apellidoField.setText(e.getApellido());
        telefonoField.setText(e.getTelefono());
        direccionField.setText(e.getDireccion());
        emailField.setText(e.getEmail());
        }catch(NullPointerException ex){
            clearSelect();
        }
        
    }

    @FXML
    private void getVendedor(MouseEvent event) {
        try{
        Empleado e = tablaVendedor.getSelectionModel().getSelectedItem();
        cedulaField.setText(e.getCedula());
        nombreField.setText(e.getNombre());
        apellidoField.setText(e.getApellido());
        telefonoField.setText(e.getTelefono());
        direccionField.setText(e.getDireccion());
        emailField.setText(e.getEmail());
        }catch(NullPointerException ex){
            clearSelect();
        }
    }
    //deselecciona los datos de la tabla si se hizo click
    @FXML
    private void clearSelect(){
        try{
        tablaVendedor.getSelectionModel().clearSelection();
        tablaDisenador.getSelectionModel().clearSelection();
        cedulaField.clear();
        nombreField.clear();
        apellidoField.clear();
        telefonoField.clear();
        direccionField.clear();
        emailField.clear();
        } catch(NullPointerException ex){
            System.out.println(ex);
        }
    }
        public void ActivarBoton(){
        nuevoEmpleado.setDisable(false);
    }
    public void DesactivarBoton(){
        nuevoEmpleado.setDisable(true);
    }

}
