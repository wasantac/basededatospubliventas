/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Conexion;
import basesObjetos.Fisico;
import basesObjetos.Material;
import basesObjetos.Servicio;
import basesObjetos.Virtual;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author walte
 */
public class NuevoProductoController implements Initializable {

    @FXML
    private TextField nombreField;
    @FXML
    private TextField precioField;
    @FXML
    private TextArea descripcionField;
    @FXML
    private RadioButton virtualRD;
    @FXML
    private ToggleGroup tipo;
    @FXML
    private RadioButton FisicoRD;
    @FXML
    private RadioButton ServicioRD;
    @FXML
    private TextField xField;
    @FXML
    private TextField yField;
    @FXML
    private Button btnCrear;
    @FXML
    private Button btnCancelar;
    @FXML
    private ChoiceBox<String> choiceTipo;
    private static ObservableList<String> virtual = FXCollections.observableArrayList("Photoshop","Ilustrator","Video");
    private static ObservableList<String> fisico = FXCollections.observableArrayList("Banner","Gigantografia","Roll Up","Plastificado");
    @FXML
    private TextField servicioField;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    @FXML
    private TextField hora;
    @FXML
    private TextField minuto;
    @FXML
    private TextField segundo;
    @FXML
    private ChoiceBox<Material> choiceMaterial;


    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        xField.setDisable(true);
        btnCrear.disableProperty().bind(Bindings.isEmpty(nombreField.textProperty())
                                        .or(Bindings.isEmpty(precioField.textProperty()))
                                        .or(Bindings.isEmpty(descripcionField.textProperty()))
                                        .or(Bindings.isEmpty(hora.textProperty()))
                                        .or(Bindings.isEmpty(minuto.textProperty()))
                                        .or(Bindings.isEmpty(segundo.textProperty())));
        yField.setDisable(true);
        virtualRD.fire();
        choiceTipo.setItems(virtual);
        choiceMaterial.setItems(Bases.material);
    }    
    //crea un nuevo produco permite que se agruege a la base de datos
    @FXML
    private void crearPorducto(ActionEvent event) throws Exception {
        String nombre = nombreField.getText();
        String precio = precioField.getText();
        String descripcion = descripcionField.getText();
        String tiempoH = hora.getText();
        String tiempoM = minuto.getText();
        String tiempoS = segundo.getText();
        String servicio = servicioField.getText();
        Time tiempo;
        if(isNumeric(tiempoH) && isNumeric(tiempoM) && isNumeric(tiempoS)){
            tiempo = new Time(Integer.parseInt(tiempoH),Integer.parseInt(tiempoM),Integer.parseInt(tiempoS));
            System.out.println(tiempo);
            if(!((nombre.equalsIgnoreCase("")) || (precio.equalsIgnoreCase("")) || (descripcion.equalsIgnoreCase("")))){
                if(virtualRD.isSelected()){
                    if(!(choiceTipo.getValue() == null) && (isNumeric(precio))){
                        System.out.println("Crear Producto Virtual");
                        int id = setId();
                        con.conectar();
                        String query = "insert into producto(idProducto,nombre,descripcion,Precio,Tiempo) values(?,?,?,?,?)";
                        String query2 = "insert into prodVirtual(idProducto,tipo) values(?,?);";
                        pst = con.getCon().prepareStatement(query);
                        pst.setInt(1,id);
                        pst.setString(5, tiempo.toString());
                        pst.setString(2, nombre);
                        pst.setString(3, descripcion);
                        pst.setDouble(4, Double.parseDouble(precio));
                        pst.execute();
                            
                        pst = con.getCon().prepareStatement(query2);
                        pst.setInt(1, id);
                        pst.setString(2, choiceTipo.getValue());
                        pst.execute();  
                        
                        Virtual v = new Virtual(setId(),tiempo,nombre,descripcion,Double.parseDouble(precio),choiceTipo.getValue());
                        System.out.println(v);
                        Bases.productoV.add(v);
                    }
                    else{
                        System.out.println("Falta Seleccionar Valor");
                    }
                }
                if(FisicoRD.isSelected()){
                    if(isNumeric(xField.getText()) && isNumeric(xField.getText())){
                        int x = parseInt(xField.getText());
                        int y = parseInt(yField.getText());
                        if(!(choiceTipo.getValue() == null)){
                            int id = setId();
                            con.conectar();
                            System.out.println("Crear Producto Fisico");
                            String query = "insert into producto(idProducto,nombre,descripcion,Precio,Tiempo) values(?,?,?,?,?)";
                            String query2 = "insert into fisico(idProducto,ancho,alto,tipo,idMaterial) values(?,?,?,?,?);";
                            pst = con.getCon().prepareStatement(query);
                            pst.setInt(1,id);
                            pst.setString(2, nombre);
                            pst.setString(3, descripcion);
                            pst.setDouble(4, Double.parseDouble(precio));
                            pst.setString(5, tiempo.toString());
                            pst.execute();

                            pst = con.getCon().prepareStatement(query2);
                            pst.setInt(1, id);
                            pst.setInt(2,x);
                            pst.setInt(3, y);
                            pst.setString(4, choiceTipo.getValue());
                            pst.setInt(5,choiceMaterial.getValue().getIdMaterial());
                            pst.execute();  
                            Fisico f =  new Fisico(id,tiempo,nombre,descripcion,Double.parseDouble(precio)* choiceMaterial.getValue().getPrecio(),x,y,choiceTipo.getValue(),choiceMaterial.getValue());
                            System.out.println(f);
                            Bases.productoF.add(f);
                            System.out.println(f.getTiempoEmpleado());
                            
                        }
                        else{
                            System.out.println("Falta Seleccionar Valor");
                        }
                    }
                    else{
                        System.out.println("Las Dimensiones no son correctas");
                    }
                }
                if(ServicioRD.isSelected()){
                    if(!servicio.equalsIgnoreCase("")){
                        System.out.println("Crear Servicio");
                        int id = setId();
                        con.conectar();
                        String query = "insert into producto(idProducto,nombre,descripcion,Precio,Tiempo) values(?,?,?,?,?)";
                        String query2 = "insert into servicio(idProducto,tipoServicio) values(?,?);";
                        pst = con.getCon().prepareStatement(query);
                        pst.setInt(1,id);
                        pst.setString(5, tiempo.toString());
                        pst.setString(2, nombre);
                        pst.setString(3, descripcion);
                        pst.setDouble(4, Double.parseDouble(precio));
                        pst.execute();
                            
                        pst = con.getCon().prepareStatement(query2);
                        pst.setInt(1, id);
                        pst.setString(2, servicioField.getText());
                        pst.execute();  
                        Servicio s =  new Servicio(setId(),tiempo,nombre,descripcion,Double.parseDouble(precio),servicio);
                        Bases.productoS.add(s);
                        System.out.println(s);
                    }
                    else{
                        System.out.println("Campo Vacio");
                    }
                }
            }
            else{
                System.out.println("Campos Vacios");
            }
        }
        else{
            System.out.println("Campo Incorrecto");
        }
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
        con.desconectar();
    }

    @FXML
    private void cancelar(ActionEvent event) {
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    private void setVirtual(ActionEvent event) {
        choiceTipo.setItems(virtual);
        choiceTipo.setVisible(true);
       servicioField.setVisible(false);
        xField.setDisable(true);
        yField.setDisable(true);
        xField.clear();
        yField.clear();
        choiceMaterial.setDisable(true);
    }

    @FXML
    private void setFisico(ActionEvent event) {
        choiceTipo.setItems(fisico);
        choiceTipo.setVisible(true);
       servicioField.setVisible(false);
        xField.setDisable(false);
        yField.setDisable(false);
        choiceMaterial.setDisable(false);
    }

    @FXML
    private void setServicio(ActionEvent event) {
       choiceTipo.setVisible(false);
       servicioField.setVisible(true);
       xField.setDisable(true);
       yField.setDisable(true);
       xField.clear();
       yField.clear();
       choiceMaterial.setDisable(true);
    }
    private int setId() throws Exception{
        con.conectar();
        int num = 1;
        String query = "select max(idProducto) from producto;" ;
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        while(rst.next()){
        num = rst.getInt(1);}
        con.desconectar();
        return num + 1;
    }
    public static boolean isNumeric(String str) { 
      try {  
        Double.parseDouble(str);  
        return true;
      } catch(NumberFormatException e){  
        return false;  
      }  
    }
}
