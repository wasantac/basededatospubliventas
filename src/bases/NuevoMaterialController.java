/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Conexion;
import basesObjetos.Material;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class NuevoMaterialController implements Initializable {

    @FXML
    private Button crearBtn;
    @FXML
    private Button cancelar;
    @FXML
    private TextField materialField;
    @FXML
    private TextField precioField;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        crearBtn.disableProperty().bind(Bindings.isEmpty(materialField.textProperty()).or(Bindings.isEmpty(precioField.textProperty())));
        
    }    
    //permite agregar un nuevo material y agregarlo a la base de datos
    @FXML
    private void crearMaterial(ActionEvent event) throws SQLException, Exception {
        String nombre = materialField.getText();
        String precio = precioField.getText();
        int id;
        if(!(nombre.equalsIgnoreCase("") || precio.equalsIgnoreCase(""))){
            if(isNumeric(precio)){
                id = getID();
                double precioValor = Double.parseDouble(precio);
                con.conectar();
                String query = "insert into material(idMaterial,nombreMaterial,Precio) values(?,?,?)";
                pst = con.getCon().prepareStatement(query);
                pst.setInt(1, id);
                pst.setString(2, nombre);
                pst.setDouble(3, precioValor);
                pst.execute(); 
                Material m = new Material(id,nombre,precioValor);
                Bases.material.add(m);
            }
            else{
                System.out.println("No es un numero");
            }
        }
        else{
            System.out.println("Campos Vacios");
        }
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }

    @FXML
    private void Cancelar(ActionEvent event) {
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }
    //obtiene el maximo id de la base para que se pueda agregar
    private int getID() throws SQLException, Exception{
        con = new Conexion();
        con.conectar();
        String query = "select max(idMaterial) from material";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        int i = 0;
        while(rst.next()){
            i = rst.getInt(1);
        }
        con.desconectar();
        return i + 1;
    }
    //verifica si es un numero
    public static boolean isNumeric(String str) { 
      try {  
        Double.parseDouble(str);  
        return true;
      } catch(NumberFormatException e){  
        return false;  
      }  
    }
}
