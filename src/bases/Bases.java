/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import static bases.ClientesController.clientes;
import static bases.ClientesController.clientesJ;
import static bases.EmpleadosController.disenador;
import static bases.EmpleadosController.vendedor;
import basesObjetos.Cargo;
import basesObjetos.ClienteJuridico;
import basesObjetos.ClienteNatural;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import basesObjetos.Conexion;
import basesObjetos.Contrato;
import basesObjetos.Empleado;
import basesObjetos.Fisico;
import basesObjetos.Material;
import basesObjetos.Pago;
import basesObjetos.Paquete;
import basesObjetos.Persona;
import basesObjetos.Servicio;
import basesObjetos.Virtual;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author walte
 */
public class Bases extends Application {
        public static ObservableList<ClienteNatural> clientes = FXCollections.observableArrayList();
        public static ObservableList<ClienteJuridico> clientesJ = FXCollections.observableArrayList();
        public static ObservableList<Empleado> disenador = FXCollections.observableArrayList();
        public static ObservableList<Empleado> vendedor = FXCollections.observableArrayList();
        public static ObservableList<Virtual> productoV = FXCollections.observableArrayList();
        public static ObservableList<Fisico> productoF = FXCollections.observableArrayList();
        public static ObservableList<Servicio> productoS = FXCollections.observableArrayList();
        public static ObservableList<Material> material = FXCollections.observableArrayList();
        public static ObservableList<Contrato> contrato = FXCollections.observableArrayList();
        public static ObservableList<Paquete> paquete = FXCollections.observableArrayList();
        public static ObservableList<Pago> pago = FXCollections.observableArrayList();
        //listas para trabajar con los datos
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        stage.setTitle("Proyecto");
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);


    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
  
    }

    
}
