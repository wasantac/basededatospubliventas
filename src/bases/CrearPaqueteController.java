/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import static bases.AgregarDetalleVentaController.p;
import basesObjetos.Conexion;
import basesObjetos.Fisico;
import basesObjetos.Paquete;
import basesObjetos.Servicio;
import basesObjetos.Virtual;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class CrearPaqueteController implements Initializable {

    @FXML
    private ComboBox<Virtual> virtualBox;
    @FXML
    private Button crearVirtualBtn;
    @FXML
    private ComboBox<Fisico> fisicoBox;
    @FXML
    private Button crearFisicoBtn;
    @FXML
    private ComboBox<Servicio> servicioBox;
    @FXML
    private Button crearServicioBtn;
    @FXML
    private TextField nombreField;
    @FXML
    private TextArea descripcionField;
    @FXML
    private Button crearBtn;
    @FXML
    private Button cancelarBtn;
    @FXML
    private  Label precio;
    private static double precioValor = 0;
    private static double precioVirtual = 0;
    private static double precioFisico = 0;
    private static double precioServicio = 0;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        precioValor = 0;
        precioVirtual = 0;
        precioFisico = 0;
        precioServicio = 0;
        virtualBox.setItems(Bases.productoV);
        fisicoBox.setItems(Bases.productoF);
        servicioBox.setItems(Bases.productoS);
        precio.setText(String.format("%.2f", precioValor));
    }    
    // sirve para crear un nuevo producto sea virtual,servicio o fisico(no importa si dice virtual, fue una copia de otro codigo)
    @FXML
    private void crearVirtual(ActionEvent event) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("NuevoProducto.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle("Nuevo Paquete");
            stage.show();
            Thread thread = new Thread(){
              public void run(){
                System.out.println("Thread Running");
                while(stage.isShowing()){
                    DesactivarBoton();
                }
                ActivarBoton();
              }

            };
            thread.start();
        } catch (IOException ex) {
            Logger.getLogger(CrearPaqueteController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //crea el paquete
    @FXML
    private void crear(ActionEvent event) throws Exception {
        Virtual v = virtualBox.getValue();
        Fisico f = fisicoBox.getValue();
        Servicio s = servicioBox.getValue();
        String nombre = nombreField.getText();
        String descripcion = descripcionField.getText();
        Paquete paquete;
        
        if((v == null) && (f == null) && (s == null)){
            System.out.println("Faltan datos");
        }
        else{
            
            con = new Conexion();
            con.conectar(); 
            pst = con.getCon().prepareStatement("start transaction;");
            pst.execute();
            int id = 0;
            String idP = "select max(idPaquete) + 1 from paquete;";
            pst = con.getCon().prepareStatement(idP);
            rst = pst.executeQuery();
            while(rst.next()){
                System.out.println("id:" + rst.getInt(1));
                if(!rst.wasNull()){
                    id = rst.getInt(1);
                }
            }
            String query = "insert into paquete(idPaquete,Precio,nombre,descripcion,Virtual_idProducto,Servicio_idProducto,Fisico_idProducto) values(?,?,?,?,?,?,?);";
            pst = con.getCon().prepareStatement(query);
            pst.setInt(1, id);
            pst.setDouble(2,precioValor);
            pst.setString(3, nombre);
            pst.setString(4, descripcion);
            
            if(v == null){
                pst.setNull(5, java.sql.Types.INTEGER);
            }
            else{
                pst.setInt(5, v.getIdProducto());
            }
            if(f == null){
                pst.setNull(7, java.sql.Types.INTEGER);
            }
            else{
                pst.setInt(7, f.getIdProducto());
            }
            if(s == null) {
                pst.setNull(6, java.sql.Types.INTEGER);
            }
            else{
                pst.setInt(6, s.getIdProducto());
            }
            pst.execute();
            pst = con.getCon().prepareStatement("commit;");
            pst.execute();
            con.desconectar();
            paquete = new Paquete(id,v,f,s,nombre,descripcion,precioValor);
            AgregarDetalleVentaController.p = paquete;
            System.out.println(paquete);
            Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.close();
        
        }

    }
    
    //cierra la ventana
    @FXML
    private void cancelar(ActionEvent event) {
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }
    private void ActivarBoton(){
        crearVirtualBtn.setDisable(false);
        crearFisicoBtn.setDisable(false);
        crearServicioBtn.setDisable(false);
    }
    private void DesactivarBoton(){
        crearVirtualBtn.setDisable(true);
        crearFisicoBtn.setDisable(true);
        crearServicioBtn.setDisable(true);
    }
    //las 3 funciones ajustan el precio automaticamente
    @FXML
    private void cambiarPrecio(ActionEvent event) {
        try{
        Virtual v = virtualBox.getValue();        
        if(precioVirtual != v.getPrecio()){
            if(precioVirtual != 0){
           precioValor -= precioVirtual;
            }
           precioVirtual = v.getPrecio();
           precioValor += v.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        try{
        Fisico f = fisicoBox.getValue();
        if(precioFisico != f.getPrecio()){
            if(precioFisico != 0){
            precioValor -= precioFisico;
            }
            precioFisico = f.getPrecio();
            precioValor += f.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        try{
        Servicio s = servicioBox.getValue();
        if(precioServicio != s.getPrecio()){
            if(precioServicio != 0){
                precioValor -= precioServicio;
            }
            precioServicio = s.getPrecio();
            precioValor += s.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
    }

    @FXML
    private void cambiarPrecioF(ActionEvent event) {
        try{
        Virtual v = virtualBox.getValue();        
        if(precioVirtual != v.getPrecio()){
            if(precioVirtual != 0){
           precioValor -= precioVirtual;
            }
           precioVirtual = v.getPrecio();
           precioValor += v.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        try{
        Fisico f = fisicoBox.getValue();
        if(precioFisico != f.getPrecio()){
            if(precioFisico != 0){
            precioValor -= precioFisico;
            }
            precioFisico = f.getPrecio();
            precioValor += f.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        try{
        Servicio s = servicioBox.getValue();
        if(precioServicio != s.getPrecio()){
            if(precioServicio != 0){
                precioValor -= precioServicio;
            }
            precioServicio = s.getPrecio();
            precioValor += s.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        
    }

    @FXML
    private void cambiarPrecioS(ActionEvent event) {
        try{
        Virtual v = virtualBox.getValue();        
        if(precioVirtual != v.getPrecio()){
            if(precioVirtual != 0){
           precioValor -= precioVirtual;
            }
           precioVirtual = v.getPrecio();
           precioValor += v.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        try{
        Fisico f = fisicoBox.getValue();
        if(precioFisico != f.getPrecio()){
            if(precioFisico != 0){
            precioValor -= precioFisico;
            }
            precioFisico = f.getPrecio();
            precioValor += f.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }
        try{
        Servicio s = servicioBox.getValue();
        if(precioServicio != s.getPrecio()){
            if(precioServicio != 0){
                precioValor -= precioServicio;
            }
            precioServicio = s.getPrecio();
            precioValor += s.getPrecio();
        }
        precio.setText(String.format("%.2f", precioValor));
        }catch(NullPointerException ex){
            System.out.println(ex);
        }

    }
}
