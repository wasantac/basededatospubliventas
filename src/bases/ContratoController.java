/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Cliente;
import basesObjetos.Conexion;
import basesObjetos.Contrato;
import basesObjetos.Detalle;
import basesObjetos.Empleado;
import basesObjetos.Pago;
import basesObjetos.Paquete;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class ContratoController implements Initializable {

    @FXML
    private TableView<Contrato> tablaContrato;
    @FXML
    private Button atras;
    @FXML
    private Button nuevoContrato;
    @FXML
    private Button eliminar;
    @FXML
    private TextField buscar;
    @FXML
    private BorderPane fondo;
    @FXML
    private TableColumn<Contrato, String> idContrato;
    @FXML
    private TableColumn<Contrato, String> FechaEntrega;
    @FXML
    private TableColumn<Contrato, Cliente> idCliente;
    @FXML
    private TableColumn<Contrato, Empleado> idVendedor;
    @FXML
    private TableColumn<Contrato, String> Descuento;
    @FXML
    private TableColumn<Contrato, String> valorPagado;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    public  ObservableList<Contrato> contrato = Bases.contrato;

    /**
     * Initializes the controller class.
     */
    @FXML
    private void changeScreen (ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       setCellTable();
       tablaContrato.setItems(contrato);
        
    } 
    //sirve para inicializar las columnas 
    private void setCellTable(){
        this.idContrato.setCellValueFactory(new PropertyValueFactory("idContrato"));
        this.FechaEntrega.setCellValueFactory(new PropertyValueFactory("FechaEntrega"));
        this.idCliente.setCellValueFactory(new PropertyValueFactory("cliente"));
        this.idVendedor.setCellValueFactory(new PropertyValueFactory("vendedor"));
        this.Descuento.setCellValueFactory(new PropertyValueFactory("Descuento"));
        this.valorPagado.setCellValueFactory(new PropertyValueFactory("precioFinal"));
    //carga los datos de la base de datos y los carga en la tabla    
    }    
    //abre una pestania nueva donde se puede generar una venta
    @FXML
    private void generarVenta(ActionEvent event) throws IOException {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("GenerarVenta.fxml"));
                    Stage stage = new Stage();
                    Scene scene = new Scene(root);
                    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                    stage.setScene(scene);
                    stage.setTitle("Nueva Venta");
                    stage.show();
                        Thread thread = new Thread(){
                       public void run(){
                         System.out.println("Thread Running");
                         while(stage.isShowing()){
                             DesactivarBoton();
                         }
                         ActivarBoton();
                       }

                     };
                     thread.start();
                } catch (IOException ex) {
                    Logger.getLogger(ContratoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }


    @FXML
    private void eliminar(ActionEvent event) throws Exception {
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        Contrato conSelect = tablaContrato.getSelectionModel().getSelectedItem();
        for(Detalle d : conSelect.getDetalles()){
            pst = con.getCon().prepareStatement("Delete from detallecontrato where Contrato_idContrato  = ?;");
            pst.setInt(1, conSelect.getIdContrato());
            pst.execute();

            
        }
        for(Detalle d : conSelect.getDetalles()){
            int idP = d.getPaquete().getIdPaquete();
            System.out.println(idP);
            pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
            pst.setInt(1, idP);
            pst.execute();
            Bases.paquete.stream().filter((p) -> (p.getIdPaquete() == idP)).forEachOrdered((p) -> {
                Bases.paquete.remove(p);
            });
        }

        pst = con.getCon().prepareStatement("Delete from pago where Contrato_idContrato  = ?;");
        pst.setInt(1, conSelect.getIdContrato());
        pst.execute();
        Bases.pago.stream().filter((p) -> (p.getIdContrato() == conSelect.getIdContrato())).forEachOrdered((p) -> {
            Bases.pago.remove(p);
        });
        pst = con.getCon().prepareStatement("Delete from contrato where idContrato = ?;");
        pst.setInt(1, conSelect.getIdContrato());
        pst.execute();
        pst = con.getCon().prepareStatement("commit;");
        Bases.contrato.remove(conSelect);
        con.desconectar();
    }
    private void ActivarBoton(){
        nuevoContrato.setDisable(false);
    }
    private void DesactivarBoton(){
        nuevoContrato.setDisable(true);
    }
}
