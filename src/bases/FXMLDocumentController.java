/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import static bases.Bases.clientes;
import static bases.Bases.clientesJ;
import static bases.Bases.disenador;
import static bases.Bases.vendedor;
import basesObjetos.Cargo;
import basesObjetos.Cliente;
import basesObjetos.ClienteJuridico;
import basesObjetos.ClienteNatural;
import basesObjetos.Usuario;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import basesObjetos.Conexion;
import basesObjetos.Contrato;
import basesObjetos.Detalle;
import basesObjetos.Empleado;
import basesObjetos.Fisico;
import basesObjetos.Material;
import basesObjetos.Paquete;
import basesObjetos.Servicio;
import basesObjetos.Virtual;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import java.sql.Time;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * FXML Controller class
 *
 * @author walte
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button login;
    @FXML
    private PasswordField pass;
    @FXML
    private TextField user;
    @FXML
    private Label error;
    @FXML
    private AnchorPane pantalla;
    
    private Stage window;
    
    private Conexion con = null;
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    @FXML
    private void changeScreen (ActionEvent event) throws IOException, Exception {
        String usuario = user.getText();
        String password = pass.getText();
        Conexion con = new Conexion();
        Usuario u = new Usuario(usuario,password);
        con.conectar();
        if(con.esUsuarioValido(u)){

            Parent root = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
            Scene scene = new Scene(root);
            window = (Stage)((Node)event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();
            window.setResizable(true);
            
            con.desconectar();
        loadFromDatabase();
        loadFromDatabaseJuridico();
        loadFromDatabaseEmpleado();
        loadFromDatabaseMaterial();
        loadFromDatabaseProductoVirtual();
        loadFromDatabaseProductoFisico();
        loadFromDatabaseProductoServicio();
        loadFromDatabaseContrato();
       
            
        }
        else{
            error.setVisible(true);
        }

    }
    @FXML
    private void enterButton(KeyEvent event){
        if(event.getCode() == KeyCode.ENTER){
            login.fire();
            event.consume();
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        DropShadow s = new DropShadow();
        login.setEffect(s);
        user.setEffect(s);
        pass.setEffect(s);


    }    

    @FXML
    private void ChangeColor(MouseEvent event) {
        DropShadow s = new DropShadow();
        login.setEffect(s);   
        login.setStyle("-fx-background-color: #FF0000");
    }

    @FXML
    private void ChangeColorBack(MouseEvent event) {
        DropShadow s = new DropShadow();
        
        login.setEffect(s);   
        login.setStyle("-fx-background-color: Gold");
    }
    public void loadFromDatabase() throws SQLException, Exception{
        con = new Conexion();
        con.conectar();
        String query  = "select * from verclientenatural;";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    ClienteNatural cl = new ClienteNatural(rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(7),rst.getString(6));
                    clientes.add(cl);
                    
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
}
    public void loadFromDatabaseJuridico() throws SQLException, Exception{
        con.conectar();
        String query  = "select * from verclientejuridico;";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        while(rst.next()){
            try {
                clientesJ.add(new ClienteJuridico(rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(7),rst.getString(6),rst.getString(8)));
                    
            } catch (SQLException ex) {
                Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();

       
}
    public void loadFromDatabaseEmpleado() throws SQLException, Exception{
        
        con.conectar();
        String query  = "select * from verempleado;";
         pst = con.getCon().prepareStatement(query);
         rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    
                    if(rst.getInt(7) == Cargo.DISENADOR.getId()){
                        Empleado e = new Empleado(rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(6),Cargo.DISENADOR.getSueldo());
                        disenador.add(e);
                    }
                    if(rst.getInt(7) == Cargo.VENDEDOR.getId()){
                        Empleado e = new Empleado(rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4),rst.getString(5),rst.getString(6),Cargo.VENDEDOR.getSueldo());
                        vendedor.add(e);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();

       
}
    public void loadFromDatabaseProductoVirtual() throws SQLException, Exception{
        con = new Conexion();
        con.conectar();
        String query  = "select * from verproductovirtual;";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    String tiempo  = rst.getString(5);
                    Virtual v = new Virtual(rst.getInt(1),java.sql.Time.valueOf(tiempo),rst.getString(2),rst.getString(3),rst.getDouble(4),rst.getString(6));
                    Bases.productoV.add(v);
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
}
    public void loadFromDatabaseProductoFisico() throws SQLException, Exception{
        con = new Conexion();
        con.conectar();
        String query  = "select * from verproductofisico";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        Iterator <Material> materiales = Bases.material.iterator();
        while(rst.next()){
                try {
                    while(materiales.hasNext()){
                        Material m = materiales.next();
                        if(rst.getInt(9) == m.getIdMaterial()){
                            String tiempo  = rst.getString(5);
                            Fisico f = new Fisico(rst.getInt(1),java.sql.Time.valueOf(tiempo),rst.getString(2),rst.getString(3),rst.getDouble(4),rst.getInt(6),rst.getInt(7),rst.getString(8),m);
                            Bases.productoF.add(f);
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
}
    public void loadFromDatabaseProductoServicio() throws SQLException, Exception{
        con = new Conexion();
        con.conectar();
        String query  = "select * from verservicio;";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    String tiempo  = rst.getString(5);
                    Servicio s = new Servicio(rst.getInt(1),java.sql.Time.valueOf(tiempo),rst.getString(2),rst.getString(3),rst.getDouble(4),rst.getString(6));
                    Bases.productoS.add(s);
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
}
    public void loadFromDatabaseMaterial() throws SQLException, Exception{
        con = new Conexion();
        con.conectar();
        String query  = "select * from material";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    Material m = new Material(rst.getInt(1),rst.getString(2),rst.getDouble(3));
                    Bases.material.add(m);
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
}
    public void loadFromDatabaseContrato() throws Exception {
        con = new Conexion();
        con.conectar();
        String queryPaquete = "select * from paquete;";
        String queryDetalles = "select * from detallecontrato;";
        String query  = "select * from contrato";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    Cliente cliente = new Cliente();
                    Empleado empleado = new Empleado();
                    for(Cliente c: Bases.clientes){
                        if(c.getCedula().equalsIgnoreCase(rst.getString(4))){
                           cliente = c;
                        }
                    }
                    for(Cliente c: Bases.clientesJ){
                        if(c.getCedula().equalsIgnoreCase(rst.getString(4))){
                           cliente = c;
                        }
                    }
                    for(Empleado e : Bases.vendedor){
                        if(e.getCedula().equalsIgnoreCase(rst.getString(5))){
                            empleado = e;
                        }
                    }
                    Contrato c = new Contrato(rst.getInt(1),rst.getDate(2),rst.getFloat(3),cliente,empleado, (float) rst.getDouble(6));
                    Bases.contrato.add(c);
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        pst = con.getCon().prepareStatement(queryPaquete);
        rst = pst.executeQuery();
        while(rst.next()){
            Virtual virtual = new Virtual();
            Fisico fisico = new Fisico();
            Servicio servicio = new Servicio();
            for(Virtual v : Bases.productoV){
                if(v.getIdProducto() == rst.getInt(5)){
                    virtual = v;
                }
            }
            for(Fisico f : Bases.productoF){
                if(f.getIdProducto() == rst.getInt(7)){
                    fisico = f;
                }
            }
            for(Servicio s: Bases.productoS){
                if(s.getIdProducto() == rst.getInt(6)){
                    servicio = s;
                }
            }
            Paquete p = new Paquete(rst.getInt(1),virtual,fisico,servicio,rst.getString(3),rst.getString(4),rst.getDouble(2));
            Bases.paquete.add(p);
        
        }
        pst = con.getCon().prepareStatement(queryDetalles);
        rst = pst.executeQuery();
        while(rst.next()){
            for(Contrato c: Bases.contrato){
                if(c.getIdContrato() == rst.getInt(5)){
                   Paquete paquete = new Paquete();
                   Empleado empleado = new Empleado();
                    for(Paquete p : Bases.paquete){
                        if(p.getIdPaquete() == rst.getInt(6)){
                            paquete = p;
                        }
                    }
                    for(Empleado d: Bases.disenador){
                        
                    }
                    c.getDetalles().add(new Detalle(rst.getInt(2),rst.getDate(3),rst.getString(4),paquete,empleado,rst.getDouble(8)));
                }
            
            }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
    }
    
}

  
