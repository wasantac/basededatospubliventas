/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import basesObjetos.Conexion;
import basesObjetos.Detalle;
import basesObjetos.Empleado;
import basesObjetos.Paquete;
import basesObjetos.Persona;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;
import java.sql.*;
import java.time.LocalDate;
import javafx.scene.control.ComboBox;
/**
 * FXML Controller class
 *
 * @author walte
 */
public class AgregarDetalleVentaController implements Initializable {

    @FXML
    private Button crearBtn;
    @FXML
    private Button cancelarBtn;
    @FXML
    private Button agregarPaqueteBtn;
    @FXML
    private Button BorrarBtn;
    @FXML
    private DatePicker publicidadDpicker;
    @FXML
    private TextField cantidadField;
    @FXML
    private ChoiceBox<String> colorBox;
    public  ObservableList<String> color = FXCollections.observableArrayList("Blanco/Negro","Color");
    public static Paquete p;
    @FXML
    private TextField paqueteField;
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    public static int id = 0;
    @FXML
    private Label empleadoLabel;
    @FXML
    private ComboBox<Empleado> empleadoBox;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colorBox.setItems(color);
        empleadoBox.setItems(Bases.disenador);
    }    
    // crea un detalle de contrato
    @FXML
    private void crear(ActionEvent event) throws Exception{ 

        String cantidadText = cantidadField.getText();
        int cantidad = Integer.valueOf(cantidadText);
        System.out.println(cantidad);
        Date duracion = Date.valueOf(publicidadDpicker.getValue());
        String color = colorBox.getValue();
        Detalle d = new Detalle(cantidad,duracion,color,p,empleadoBox.getValue(),(p.getPrecioPaquete())*cantidad);
        GenerarVentaController.detalles.add(d);
        GenerarVentaController.precioFinal += cantidad * p.getPrecioPaquete();
        System.out.println(GenerarVentaController.precioFinal);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
        
    }
    // cierra la ventana
    @FXML
    private void cancelar(ActionEvent event) {
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.close();
    }
    //abre una nueva ventana para agrega y crear un paquete
    @FXML
    private void agregarPaquete(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("CrearPaquete.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("Nuevo Paquete");
        stage.show();
        Thread thread2 = new Thread(){
          public void run(){
            System.out.println("Thread Running");
            while(stage.isShowing()){
                DesactivarBoton();
            }
            ActivarBoton();
            try{
            paqueteField.setText(p.getNombrePaquete());
            }
            catch(NullPointerException ex){
                System.out.println(ex);
            }
          }
          
        };
        thread2.start();
    }

    //borra el paquete creado
    @FXML
    private void borrarPaquete(ActionEvent event) {
        paqueteField.clear();
        p = null;
    }
        private void ActivarBoton(){
        agregarPaqueteBtn.setDisable(false);
    }
    private void DesactivarBoton(){
        agregarPaqueteBtn.setDisable(true);
    }
}


