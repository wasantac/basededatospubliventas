/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import static bases.Bases.clientes;
import basesObjetos.Cliente;
import basesObjetos.ClienteNatural;
import basesObjetos.Conexion;
import basesObjetos.Contrato;
import basesObjetos.Detalle;
import basesObjetos.Empleado;
import basesObjetos.Material;
import basesObjetos.Pago;
import basesObjetos.Paquete;
import basesObjetos.Persona;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class GenerarVentaController implements Initializable {

    @FXML
    private Label valorFinal;
    @FXML
    private TextField CedulaClienteField;
    @FXML
    private TextField CedulaVendedorField;
    @FXML
    private DatePicker entregaDPicker;
    @FXML
    private ComboBox<String> PagoBox;
    @FXML
    private TextField MontoField;
    @FXML
    private TableView<Detalle> tablaDetalleVenta;
    @FXML
    private TableColumn<Detalle, String> Cantidad;
    @FXML
    private TableColumn<Detalle, String> DuracionPublicidad;
    @FXML
    private TableColumn<Detalle, String> Color;
    @FXML
    private TableColumn<Detalle, Paquete> Paquete;
    @FXML
    private Button venderBtn;
    @FXML
    private Button aggBtn;
    @FXML
    private Button eliminarBtn;
    public  ObservableList<String> cedulaCliente = FXCollections.observableArrayList();
    public  ObservableList<String> cedulaVendedor = FXCollections.observableArrayList();
    public static  ObservableList<Detalle> detalles = FXCollections.observableArrayList();
    public  ObservableList<String> formaPago = FXCollections.observableArrayList("Tarjeta De Credito","Contado");
    private Conexion con = new Conexion();
    private PreparedStatement pst = null;
    private ResultSet rst = null;
    public static float precioFinal = 0;
    @FXML
    private TableColumn<Detalle, Persona> empleadoResponsable;
    @FXML
    private TableColumn<Detalle, String> precioDetalle;
    @FXML
    private TextField descuentoField;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            crearListaCedulas();
            crearListaCedulasVendedor();
        } catch (Exception ex) {
            Logger.getLogger(GenerarVentaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        TextFields.bindAutoCompletion(CedulaClienteField,cedulaCliente);
        TextFields.bindAutoCompletion(CedulaVendedorField,cedulaVendedor);
        PagoBox.setItems(formaPago);
        setCellTable();
        tablaDetalleVenta.setItems(detalles);
        descuentoField.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("textfield changed from " + oldValue + " to " + newValue);
                precioFinal = 0;
                for(Detalle d : detalles){
                    precioFinal += d.getPrecio();
                }
                if(isNumeric(descuentoField.getText())){                
                    float descuento = Float.parseFloat(descuentoField.getText());
                    valorFinal.setText(Float.toString(precioFinal*descuento));
                }
                else{
                    valorFinal.setText(Float.toString(precioFinal));
                }
        });
    }    

    //crea un contrato y agrega todos los detalles a la base de datos
    @FXML
    private void vender(ActionEvent event) throws Exception {
        if(isNumeric(MontoField.getText())){
            double monto = Double.parseDouble(MontoField.getText());
            if(isNumeric(descuentoField.getText())){
                float descuento = Float.parseFloat(descuentoField.getText());
                if(descuento <= 1){
                    precioFinal = precioFinal *descuento;
                    if(monto > precioFinal){
                        con = new Conexion();
                        con.conectar();
                        int id = 1;
                        pst = con.getCon().prepareStatement("select Max(idContrato) + 1 from contrato;");
                        rst = pst.executeQuery();
                        while(rst.next()){
                         id = rst.getInt(1);
                        }
                        String transact = "start transaction;";
                        pst = con.getCon().prepareStatement(transact);
                        pst.execute();
                        String query = "insert into contrato(idContrato,fechaEntrega,Descuento,Cliente_Cedula,Empleado_Cedula,precioFinal) values(?,?,?,?,?,?);";
                        pst = con.getCon().prepareStatement(query);
                        Date entrega = Date.valueOf(entregaDPicker.getValue());
                        pst.setInt(1, id);
                        pst.setDate(2, entrega);
                        pst.setFloat(3, descuento);
                        pst.setString(4, CedulaClienteField.getText());
                        pst.setString(5, CedulaVendedorField.getText());
                        pst.setDouble(6, precioFinal);
                        pst.execute();
                        for(Detalle d: detalles){
                            String query2 = "insert into detallecontrato(cantidad,duracionPublicidad,Color,Contrato_idContrato,Paquete_idPaquete,EmpleadoResponsable,precioDetalle) values(?,?,?,?,?,?,?);";
                            pst = con.getCon().prepareStatement(query2);
                            pst.setInt(1, d.getCantidad());
                            pst.setDate(2, d.getDuracionPublicidad());
                            pst.setString(3, d.getColor());
                            pst.setInt(4, id);
                            pst.setInt(5, d.getPaquete().getIdPaquete());
                            pst.setString(6, d.getEmpleadoResponsable().getCedula());
                            pst.setDouble(7, d.getPrecio());
                            pst.execute();

                        }

                        while(rst.next()){
                            id = rst.getInt(1);
                        }
                        Cliente cliente = null;
                        Empleado empleado = null;
                        for(Cliente cli: Bases.clientes){
                            if(cli.getCedula().equalsIgnoreCase(CedulaClienteField.getText())){
                                cliente = cli;
                            }
                        }
                        for(Cliente cli: Bases.clientesJ){
                            if(cli.getCedula().equalsIgnoreCase(CedulaClienteField.getText())){
                                cliente = cli;
                            }
                        }
                        for(Empleado e : Bases.vendedor){
                            if(e.getCedula().equalsIgnoreCase(CedulaVendedorField.getText())){
                                empleado = e;
                            }
                        }
                        Contrato c = new Contrato(id,entrega,1,cliente,empleado,precioFinal, detalles);
                        Bases.contrato.add(c);

                        pst = con.getCon().prepareStatement("insert into pago(monto,formaPago,Contrato_idContrato) values(?,?,?);");
                        pst.setDouble(1,monto);
                        pst.setString(2, PagoBox.getValue());
                        pst.setInt(3, id);
                        pst.execute();

                        pst = con.getCon().prepareStatement("select max(idPago) +1 from pago");
                        rst = pst.executeQuery();
                        while (rst.next()){
                            if(!rst.wasNull()){
                                Pago p = new Pago(rst.getInt(1),id,monto,PagoBox.getValue());
                                Bases.pago.add(p);
                            }
                            else{
                                Pago p = new Pago(1,id,monto,PagoBox.getValue());
                                Bases.pago.add(p);

                            }

                        }
                        pst = con.getCon().prepareStatement("commit;");
                        pst.execute();

                        con.desconectar();

                        detalles.clear();
                        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                        window.close();
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Venta");
                        alert.setHeaderText(null);
                        String mensaje = "Vuelto: " + (monto - precioFinal); 
                        alert.setContentText(mensaje);

                        alert.showAndWait();
                            }
                    else{System.out.println("El monto a pagar es menor");}
                        }
                    }
                else if(descuentoField.getText().isEmpty()){
                    con = new Conexion();
                    con.conectar();
                    int id = 1;
                    pst = con.getCon().prepareStatement("select Max(idContrato) + 1 from contrato;");
                    rst = pst.executeQuery();
                    while(rst.next()){
                     id = rst.getInt(1);
                    }
                    String transact = "start transaction;";
                    pst = con.getCon().prepareStatement(transact);
                    pst.execute();
                    String query = "insert into contrato(idContrato,fechaEntrega,Descuento,Cliente_Cedula,Empleado_Cedula,precioFinal) values(?,?,?,?,?,?);";
                    pst = con.getCon().prepareStatement(query);
                    Date entrega = Date.valueOf(entregaDPicker.getValue());
                    pst.setInt(1, id);
                    pst.setDate(2, entrega);
                    pst.setFloat(3, 1);
                    pst.setString(4, CedulaClienteField.getText());
                    pst.setString(5, CedulaVendedorField.getText());
                    pst.setDouble(6, precioFinal);
                    pst.execute();


                    for(Detalle d: detalles){
                        String query2 = "insert into detallecontrato(cantidad,duracionPublicidad,Color,Contrato_idContrato,Paquete_idPaquete,EmpleadoResponsable,precioDetalle) values(?,?,?,?,?,?,?);";
                        pst = con.getCon().prepareStatement(query2);
                        pst.setInt(1, d.getCantidad());
                        pst.setDate(2, d.getDuracionPublicidad());
                        pst.setString(3, d.getColor());
                        pst.setInt(4, id);
                        pst.setInt(5, d.getPaquete().getIdPaquete());
                        pst.setString(6, d.getEmpleadoResponsable().getCedula());
                        pst.setDouble(7, d.getPrecio());
                        pst.execute();

                    }


                    while(rst.next()){
                        id = rst.getInt(1);
                    }
                    Cliente cliente = null;
                    Empleado empleado = null;
                    for(Cliente cli: Bases.clientes){
                        if(cli.getCedula().equalsIgnoreCase(CedulaClienteField.getText())){
                            cliente = cli;
                        }
                    }
                    for(Cliente cli: Bases.clientesJ){
                        if(cli.getCedula().equalsIgnoreCase(CedulaClienteField.getText())){
                            cliente = cli;
                        }
                    }
                    for(Empleado e : Bases.vendedor){
                        if(e.getCedula().equalsIgnoreCase(CedulaVendedorField.getText())){
                            empleado = e;
                        }
                    }
                    Contrato c = new Contrato(id,entrega,1,cliente,empleado,precioFinal, detalles);
                    Bases.contrato.add(c);

                    pst = con.getCon().prepareStatement("insert into pago(monto,formaPago,Contrato_idContrato) values(?,?,?);");
                    pst.setDouble(1,monto);
                    pst.setString(2, PagoBox.getValue());
                    pst.setInt(3, id);
                    pst.execute();

                    pst = con.getCon().prepareStatement("select max(idPago) +1 from pago");
                    rst = pst.executeQuery();
                    while (rst.next()){
                        if(!rst.wasNull()){
                            Pago p = new Pago(rst.getInt(1),id,monto,PagoBox.getValue());
                            Bases.pago.add(p);
                        }
                        else{
                            Pago p = new Pago(1,id,monto,PagoBox.getValue());
                            Bases.pago.add(p);

                        }

                    }
                    pst = con.getCon().prepareStatement("commit;");
                    pst.execute();

                    con.desconectar();

                    detalles.clear();
                    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                    window.close();
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Venta");
                    alert.setHeaderText(null);
                    String mensaje = "Vuelto: " + (monto - precioFinal); 
                    alert.setContentText(mensaje);

                    alert.showAndWait();
                }
                else{System.out.println("Error");}
    }
        else{System.out.println("No es un mumero");}
}
    


    @FXML
    private void eliminar(ActionEvent event) throws Exception {
        con.conectar();
        pst = con.getCon().prepareStatement("start transaction;");
        pst.execute();
        Detalle d = tablaDetalleVenta.getSelectionModel().getSelectedItem();
        Iterator<Detalle> detalleIt = detalles.iterator();
        while(detalleIt.hasNext()){
            Detalle det = detalleIt.next();
            if(d == det ){
                int idP = d.getPaquete().getIdPaquete();
                System.out.println(idP);
                pst = con.getCon().prepareStatement("Delete from paquete where idPaquete = ?;");
                pst.setInt(1, idP);
                pst.execute();
                detalleIt.remove();
            }
            
        }
        pst = con.getCon().prepareStatement("commit;");
        pst.execute();
        con.desconectar();
    }

    //sirve para busqueda
    private void crearListaCedulas() throws Exception{
        con = new Conexion();
        con.conectar();
        String query  = "Select persona.Cedula from persona " +
                        "join cliente on cliente.Cedula = persona.Cedula;";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    
                    String s = rst.getString(1);
                    cedulaCliente.add(s);
                    
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
    }
    //sirve para busqueda
    private void crearListaCedulasVendedor() throws Exception{
        con = new Conexion();
        con.conectar();
        String query  = "Select persona.Cedula from persona " +
                        "join empleado on empleado.Cedula = persona.Cedula " +
                        "join cargo on empleado.Cargo_idCargo = cargo.idCargo " +
                        "where cargo.idCargo = 2;";
        pst = con.getCon().prepareStatement(query);
        rst = pst.executeQuery();
        
        while(rst.next()){
                try {
                    
                    String s = rst.getString(1);
                    cedulaVendedor.add(s);
                    
                } catch (SQLException ex) {
                    Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        System.out.println("Tabla Cargada Exitosamente");
        con.desconectar();      
    }
    //inicializa las columnas de la tabla
    private void setCellTable(){
        this.Cantidad.setCellValueFactory(new PropertyValueFactory("Cantidad"));
        this.DuracionPublicidad.setCellValueFactory(new PropertyValueFactory("DuracionPublicidad"));
        this.Color.setCellValueFactory(new PropertyValueFactory("Color"));
        this.Paquete.setCellValueFactory(new PropertyValueFactory("Paquete"));
        this.empleadoResponsable.setCellValueFactory(new PropertyValueFactory("EmpleadoResponsable"));
        this.precioDetalle.setCellValueFactory(new PropertyValueFactory("Precio"));
         
    //carga los datos de la base de datos y los carga en la tabla    
    }
    //abre una nueva ventana para agregar detalles
    @FXML
    private void agregarDetalle(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("AgregarDetalleVenta.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("Nuevo Detalle");
        stage.show();
        Thread thread = new Thread(){
          public void run(){
            System.out.println("Thread Running");
            while(stage.isShowing()){
                
                aggBtn.setDisable(true);
            }
                    Platform.runLater(new Runnable() {
            @Override public void run() {
                precioFinal = 0;
                for(Detalle d : detalles){
                    precioFinal += d.getPrecio();
                }
                if(isNumeric(descuentoField.getText())){                
                    float descuento = Float.parseFloat(descuentoField.getText());
                    valorFinal.setText(Float.toString(precioFinal*descuento));
                }
                else{
                    valorFinal.setText(Float.toString(precioFinal));
                }
            }
        });
            aggBtn.setDisable(false);
            
          } 
        };
        thread.start();
        
                   /* float precio = 0;
             for (int i = 0; i < tablaDetalleVenta.getItems().size(); i++){
                 for (TableColumn<Detalle, ?> pqte : Paquete.getColumns()){
                    Paquete cellData = (Paquete) pqte.getCellData(i);
                    precio += cellData.getPrecioPaquete();
                     System.out.println(precio);
                 }
             }
             valorFinal.setText(Float.toString(precio));
*/
    }

        public static boolean isNumeric(String str) { 
      try {  
        Double.parseDouble(str);  
        return true;
      } catch(NumberFormatException e){  
        return false;  
      }  
    }
}
