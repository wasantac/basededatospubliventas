/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bases;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author walte
 */
public class MainScreenController implements Initializable {


    @FXML
    private Button empleado;
    @FXML
    private Button producto;
    @FXML
    private Button cliente;
    @FXML
    private Button contrato;
    @FXML
    private BorderPane pantalla;
    
    //pantalla principal con todas las opciones

    @FXML
    private void changeScreen (ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.close();
        System.exit(0);
    }
    @FXML
    private void contratoScreen (ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Contrato.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
        @FXML
    private void clienteScreen (ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Clientes.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
    }    

    @FXML
    private void empleadoScreen(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Empleados.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    @FXML
    private void productoScreen(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Productos.fxml"));
        Scene scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
    
}
