/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;
import java.sql.*;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 *
 * @author walte
 */
public class Contrato {
    private int idContrato;
    private Date fechaEntrega;
    private float descuento;
    private Cliente cliente;
    private Empleado vendedor;
    private ObservableList<Detalle> detalles;
    private float precioFinal;

    public Contrato(int idContrato, Date fechaEntrega, float descuento, Cliente cliente, Empleado vendedor,float precioFinal,ObservableList<Detalle> detalles) {
        this.idContrato = idContrato;
        this.fechaEntrega = fechaEntrega;
        this.descuento = descuento;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.detalles = detalles;
        this.precioFinal = precioFinal;
    }

    public Contrato(int idContrato, Date fechaEntrega, Cliente cliente, Empleado vendedor,ObservableList<Detalle> detalles) {
        this.idContrato = idContrato;
        this.fechaEntrega = fechaEntrega;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.descuento = 1;
        this.detalles = detalles;
    }

    public Contrato(int idContrato, Date fechaEntrega, float descuento, Cliente cliente, Empleado vendedor, float precioFinal) {
        this.idContrato = idContrato;
        this.fechaEntrega = fechaEntrega;
        this.descuento = descuento;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.precioFinal = precioFinal;
        this.detalles = FXCollections.observableArrayList(); 
    }

    public Contrato() {
        
    }
    

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float Descuento) {
        this.descuento = Descuento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Empleado getVendedor() {
        return vendedor;
    }

    public void setVendedor(Empleado vendedor) {
        this.vendedor = vendedor;
    }

    public ObservableList<Detalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(ObservableList<Detalle> detalles) {
        this.detalles = detalles;
    }

    public float getPrecioFinal() {
        return precioFinal;
    }

    public void setPrecioFinal(float precioFinal) {
        this.precioFinal = precioFinal;
    }
    
    
    
}
