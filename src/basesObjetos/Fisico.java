/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

import java.sql.Time;

/**
 *
 * @author walte
 */
public class Fisico extends Producto{
    private int ancho;
    private int alto;
    private String tipo;
    private Material material;
    public Fisico(int idProducto, Time TiempoEmpleado, String nombre, String descripcion, double precio,int ancho,int alto,String tipo,Material material) {
        super(idProducto, TiempoEmpleado, nombre, descripcion, precio);
        this.ancho = ancho;
        this. alto = alto;
        this.tipo = tipo;
        this.material = material;
    }

    public Fisico() {
        
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getAlto() {
        return alto;
    }

    public void setAlto(int alto) {
        this.alto = alto;
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Fisico{" + "dimensiones=" + (ancho*alto) + ", tipo=" + tipo + '}';
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    
    
    
}
