/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import bases.FXMLDocumentController;

/**
 *
 * @author walte
 */
public class Conexion {
    private Connection con;
    private static final String DRIVER   = "com.mysql.cj.jdbc.Driver";
    private static final String DBMS     = "mysql";
    private static final String HOST     = "127.0.0.1";
    private static final String PORT     = "3306";
    private static final String DATABASE = "publiventas";
    private static final String USER     = "admin";
    private static final String PASSWORD = "admin12345";


    public Conexion() {
    }

    public Connection getCon() {
        return con;
    }
    
    /*METODO CONECTAR*/
    public void conectar ()throws Exception{
        try{
            Class.forName(DRIVER);
            this.con = DriverManager.getConnection("jdbc:" + DBMS + "://" + HOST + ":" + PORT + "/" + DATABASE + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", USER, PASSWORD);                
        }catch(Exception e){
            System.out.println("[Error] No se pudo conectar a la base de datos: "+e);
            throw e;
        } 
    System.out.println("[Info] Conexión exitosa a la base de datos");
    }
    
    public boolean desconectar()
    {
        try {
            this.con.close();
            return(true);
        }
        catch(Exception e) {
            return(false);
        }    
    }
    
    /*Método para cosultar si un usuario está registrado en la base de datos de la aplicación
    y además está activo (A)*/
    public boolean esUsuarioValido(Usuario u) { 
        boolean resultado = false;
        ResultSet rs = null;                       
        PreparedStatement st = null;
        try {            
            st = con.prepareStatement("SELECT * FROM usuario WHERE nombre = ? AND clave = ? AND estado = ?");            
            st.setString(1,u.getUser());         
            st.setString(2,u.getPassword());
            st.setString(3,"A");
            rs = st.executeQuery();            
            if(rs.next()){
                //u.setRol(rs.getString("rol").charAt(0));
                resultado = true;
            }
            rs.close();
            st.close();
        } catch(Exception e){
            System.out.println(e);
            resultado = false;
        }           
        return resultado; 
    }
}
