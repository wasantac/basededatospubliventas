/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

/**
 *
 * @author walte
 */
public class Paquete {
    private Virtual virtual;
    private Fisico fisico;
    private Servicio servicio;
    private String nombrePaquete;
    private String descripcionPaquete;
    private double precioPaquete;
    private int idPaquete;

    public Paquete(int idPaquete,Virtual virtual, Fisico fisico, Servicio servicio, String nombrePaquete, String descripcionPaquete, double precioPaquete) {
        this.virtual = virtual;
        this.fisico = fisico;
        this.servicio = servicio;
        this.nombrePaquete = nombrePaquete;
        this.descripcionPaquete = descripcionPaquete;
        this.precioPaquete = precioPaquete;
        this.idPaquete = idPaquete;
    }

    public Paquete() {
    }

    public Virtual getVirtual() {
        return virtual;
    }

    public void setVirtual(Virtual virtual) {
        this.virtual = virtual;
    }

    public Fisico getFisico() {
        return fisico;
    }

    public void setFisico(Fisico fisico) {
        this.fisico = fisico;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public String getNombrePaquete() {
        return nombrePaquete;
    }

    public void setNombrePaquete(String nombrePaquete) {
        this.nombrePaquete = nombrePaquete;
    }

    public String getDescripcionPaquete() {
        return descripcionPaquete;
    }

    public void setDescripcionPaquete(String descripcionPaquete) {
        this.descripcionPaquete = descripcionPaquete;
    }

    public double getPrecioPaquete() {
        return precioPaquete;
    }

    public void setPrecioPaquete(double precioPaquete) {
        this.precioPaquete = precioPaquete;
    }

    @Override
    public String toString() {
        return nombrePaquete;
    }

    public int getIdPaquete() {
        return idPaquete;
    }

    public void setIdPaquete(int idPaquete) {
        this.idPaquete = idPaquete;
    }
    
    
}
