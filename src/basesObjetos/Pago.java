/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

/**
 *
 * @author walte
 */
public class Pago {
    private double monto;
    private String formaPago;
    private int idContrato;
    private int idPago;

    public Pago(int idPago,int idContrato,double monto, String formaPago) {
        this.monto = monto;
        this.formaPago = formaPago;
        this.idContrato = idContrato;
        this.idPago = idPago;
    }

    public Pago() {
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public int getIdPago() {
        return idPago;
    }

    public void setIdPago(int idPago) {
        this.idPago = idPago;
    }
    
    
}
