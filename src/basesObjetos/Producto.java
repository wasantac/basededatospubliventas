/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;
import java.sql.*;
/**
 *
 * @author walte
 */
public class Producto {
    private int idProducto;
    private Time TiempoEmpleado;
    private String nombre;
    private String descripcion;
    private double precio;

    public Producto(int idProducto, Time TiempoEmpleado, String nombre, String descripcion, double precio) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        Time t = new Time(TiempoEmpleado.getHours(),TiempoEmpleado.getMinutes(),TiempoEmpleado.getSeconds());
        this.TiempoEmpleado = t;
    }

    public Producto() {
    }
    

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public Time getTiempoEmpleado() {
        return TiempoEmpleado;
    }

    public void setTiempoEmpleado(Time TiempoEmpleado) {
        this.TiempoEmpleado = TiempoEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Producto{" + "idProducto=" + idProducto + ", TiempoEmpleado=" + TiempoEmpleado + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio + '}';
    }
    
    
    
}
