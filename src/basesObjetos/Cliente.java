/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

import javafx.collections.ObservableList;

/**
 *
 * @author walte
 */
public class Cliente extends Persona{
    private String actividadComercial;
    private String nombreComercial;

    
   public Cliente(String cd, String nm, String ap, String tf, String d,String actividadComercial,String nombreComercial){
       super(cd,nm,ap,tf,d);
       this.actividadComercial = actividadComercial;
       this.nombreComercial = nombreComercial;

   }
   public Cliente(){
       super();
       this.actividadComercial = "";
       this.nombreComercial = "Consumidor Final";
   }
   
    public String getActividadComercial() {
        return actividadComercial;
    }

    public void setActividadComercial(String actividadComercial) {
        this.actividadComercial = actividadComercial;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    
    

    
}
