/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

import java.sql.Time;

/**
 *
 * @author walte
 */
public class Virtual extends Producto{
    private String tipo;
    public Virtual(int idProducto, Time TiempoEmpleado, String nombre, String descripcion, double precio,String tipo) {
        super(idProducto, TiempoEmpleado, nombre, descripcion, precio);
        this.tipo = tipo;
    }

    public Virtual() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return  tipo ;
    }
    
    
}
