/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

/**
 *
 * @author walte
 */
public class ClienteJuridico extends Cliente{
    private String razonSocial;
    public ClienteJuridico(String cd, String nm, String ap, String tf, String d,String actividadComercial,String nombreComercial,String razonSocial){
    super(cd,nm,ap,tf,d,actividadComercial,nombreComercial);
    this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
}
