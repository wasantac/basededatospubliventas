/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

/**
 *
 * @author walte
 */
public enum Cargo {
    DISENADOR(1,"DISENADOR",500),
    VENDEDOR(2,"VENDEDOR",400);
    private final int id;
    private final String empleado;
    private final float sueldo;
    
    Cargo(int id,String empleado,float sueldo){
        this.id = id;
        this.empleado = empleado;
        this.sueldo = sueldo;
    }

    public int getId() {
        return id;
    }

    public String getEmpleado() {
        return empleado;
    }

    public float getSueldo() {
        return sueldo;
    }
    
    
}
