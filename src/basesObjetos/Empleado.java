/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

/**
 *
 * @author walte
 */
public class Empleado extends Persona{
    private String email;
    private float sueldo;

    public Empleado(String cd, String nm, String ap, String tf, String d,String email,float sueldo){
        super(cd,nm,ap,tf,d);
        this.email = email;
        this.sueldo = sueldo;
    }

    public Empleado() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }
    

}
