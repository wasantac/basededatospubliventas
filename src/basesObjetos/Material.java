/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;

/**
 *
 * @author walte
 */
public class Material {
    private int idMaterial;
    private String nombreMaterial;
    private double precio;

    public Material(int idMaterial, String nombreMaterial,double precio) {
        this.idMaterial = idMaterial;
        this.nombreMaterial = nombreMaterial;
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public int getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(int idMaterial) {
        this.idMaterial = idMaterial;
    }

    public String getNombreMaterial() {
        return nombreMaterial;
    }

    public void setNombreMaterial(String nombreMaterial) {
        this.nombreMaterial = nombreMaterial;
    }

    @Override
    public String toString() {
        return  nombreMaterial;
    }
    
    
}
