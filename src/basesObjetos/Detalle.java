/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basesObjetos;
import java.sql.*;
/**
 *
 * @author walte
 */
public class Detalle {
    private int cantidad;
    private Date duracionPublicidad;
    private String Color;
    private Double precio;
    private Paquete paquete;
    private Empleado empleadoResponsable;

    public Detalle( int cantidad, Date duracionPublicidad, String Color, Paquete paquete, Empleado empleadoResponsable,Double precio) {
        this.cantidad = cantidad;
        this.duracionPublicidad = duracionPublicidad;
        this.Color = Color;
        this.paquete = paquete;
        this.empleadoResponsable = empleadoResponsable;
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getDuracionPublicidad() {
        return duracionPublicidad;
    }

    public void setDuracionPublicidad(Date duracionPublicidad) {
        this.duracionPublicidad = duracionPublicidad;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }


    public Paquete getPaquete() {
        return paquete;
    }

    public void setPaquete(Paquete paquete) {
        this.paquete = paquete;
    }

    public Empleado getEmpleadoResponsable() {
        return empleadoResponsable;
    }

    public void setEmpleadoResponsable(Empleado empleadoResponsable) {
        this.empleadoResponsable = empleadoResponsable;
    }
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
    
    
}
